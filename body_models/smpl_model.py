from human_body_prior.body_model.body_model import BodyModel
import torch.nn as nn
import torch
from utils.representation import convertPose

SMPLH_JOINTS_BODY = 22
SMPLH_JOINTS_HAND = 30

class SMPLH(nn.Module):
    """ SMPLH body model based on the body model BodyModel implementation of human body prior"""
    
    def __init__(self, configuration):
        """
        Args:
            configuration (dict): Gives information on the location of the body_model information
        """
        super(SMPLH, self).__init__()

        self.num_betas = configuration["body_model"]["num_betas"]
        self.num_dmpls = configuration["body_model"]["num_dmpls"] 
        
        if(self.num_dmpls == 0):
            self.num_dmpls = None
        self.device = configuration["device"]

        self.body_model = BodyModel(configuration["body_model"]["smpl_info"],
                                    num_betas=self.num_betas,
                                    num_dmpls=self.num_dmpls,
                                    dmpl_fname=configuration["body_model"]["dmpl_info"]).to(self.device)

    def forward(self, data, angle_repr={"name": "aa", "dim": 3}, joints=list(range(SMPLH_JOINTS_BODY)), squeeze=True, 
        use_hands=False, trans=True, Jtr=False, pose2rot=False):
        """Process the batch of smpl data and ouputs a batch of meshes

        Args:
            data ([dict]): dictionnary representing a batch of smpl data
            angle_repr (dict, optional): Angle representation of the input data. Defaults to {"name": "aa", "dim": 3}, see utils.representation.
            joints ([list], optional): Subset of joints to consider, missing joint rotation set to I. Defaults to list(range(SMPLH_JOINTS_BODY)).
            use_hands (bool, optional): Whether or not the hand pose is given in the data. Defaults to False.

        Returns:
            [tensor]: The SMPL meshes computed from the input data BSx6890x3
        """
        
        if(len(data["poses"].shape) == 1):  # if single frame and no batch
            data2 = dict()

            for k in ["poses", "trans","betas"]:
                data2[k] = data[k][None,...]
        else:
            data2 = data

        data2["dmpls"] = None
        batch_size = data2["poses"].shape[0]
        
        if(len(data2["betas"].shape) != len(data2["poses"].shape)):
            data2["betas"] = data2["betas"][None, :].repeat(batch_size, 1)

        poses = data2["poses"]

        if(angle_repr["name"] != "aa" and pose2rot):
            poses = convertPose(poses, angle_repr, {"name": "aa", "dim": 3})
          
        elif(angle_repr["name"] != "mat" and not pose2rot):
            poses = convertPose(poses, angle_repr, {"name": "mat", "dim": 9})
         
        if(pose2rot):
            dim = 3
            if(not use_hands):
                pose_hand = torch.zeros(
                    (poses.shape[0], SMPLH_JOINTS_HAND*dim), dtype=torch.float32, device=self.device)
            else:
                pose_hand = poses[:, SMPLH_JOINTS_BODY*dim:]  
            full_body_pose = torch.zeros(
                (poses.shape[0], SMPLH_JOINTS_BODY,dim), dtype=torch.float32, device=self.device)
        else:
            dim = 9
            if(not use_hands):
            
                pose_hand = torch.eye(3,3, dtype=torch.float32, device=self.device).reshape(-1)[None,None].repeat(poses.shape[0], SMPLH_JOINTS_HAND,1).reshape(poses.shape[0],-1)
               
            else:
                pose_hand = poses[:, SMPLH_JOINTS_BODY*dim:]
            
            full_body_pose = torch.eye(3,3, dtype=torch.float32, device=self.device).reshape(-1)[None,None].repeat(poses.shape[0], SMPLH_JOINTS_BODY,1)
        poses_rs = poses.view(poses.shape[0],-1,dim)
        
        full_body_pose[:,joints,:]=poses_rs
        full_body_pose = full_body_pose.reshape(poses.shape[0],-1)

        if(not trans):
            data2["trans"] = torch.zeros(
                    data2["trans"].shape, device=data2["trans"].device, dtype=data2["trans"].dtype)

        model = self.body_model(
            root_orient=full_body_pose[:, 0:dim],
            pose_body=full_body_pose[:, dim:SMPLH_JOINTS_BODY*dim],
            pose_hand=pose_hand,
            betas=data2["betas"][:, :self.num_betas],
            trans=data2["trans"],
            dmpls=data2["dmpls"],
            pose2rot=pose2rot)

        if(Jtr):
            if(squeeze):
                return model.v.reshape(-1, 6890, 3).squeeze(), model.Jtr.reshape(-1, 52, 3)
            else:
                return model.v.reshape(-1, 6890, 3), model.Jtr.reshape(-1, 52, 3)
        else:
            if(squeeze):
                return model.v.reshape(-1, 6890, 3).squeeze()
            else:
                return model.v.reshape(-1, 6890, 3)

    def get_faces(self):
        return self.body_model.f

    def to(self, device):
        self.device = device
        self.body_model = self.body_model.to(device)
        return self
