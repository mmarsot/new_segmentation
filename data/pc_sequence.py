import torch
import trimesh
from human_body_prior.tools.omni_tools import makepath
import os
import glob

class PC_Sequence:
    """Class encapsulating a pointcloud sequence with timestamps"""

    def __init__(self, folder,
                 dtype=torch.float32, device="cuda",
                 timestamps=None,   end=None, link=True, start=None, relative=False,ext="ply"):
        """Initialize a sequence

        Args:
            folder (str): path to the pointclouds files, pointcloud name format is "model-%05d.%s"%(i,ext)
            dtype: Defaults to torch.float32.
            device: Defaults to "cuda".
            timestamps : If None sequence is assumed to be a complete 50fps sequence. 
            Otherwise it should be a tensor of timestamp
            start (int): starting frame
            end (int) : Last model to consider, If relative is true, end=start+end
            link : boolean for folders containing a link to the pointcloud files in origin_folder.txt, 
            and a frame interval in limits.pt

        Raises:
            KeyError: [description]
        """

        self.timestamps = timestamps
        self.device = device
        self.dtype = dtype

        self.folder = folder
        
        if(link):
            with open(os.path.join(self.folder, "origin_folder.txt")) as f:
                self.origin_folder = f.readline()
        else:
            self.origin_folder = self.folder

       
        if(link):
            self.limits = torch.load(os.path.join(folder, "limits.pt"))
        else:
            number_of_pc = len(glob.glob(folder+"model-*"))
            self.limits = torch.tensor([0, int(number_of_pc)])
            
        if start is None:
            start = self.limits[0]
        else:
            self.limits[0] = start
        
        if(end is None):
            end = self.limits[1]
        else:
            if relative:
                self.limits[1] = self.limits[0]+end
            else:
                self.limits[1] = end
       

        self.meshes = [trimesh.load(os.path.join(
            self.origin_folder,  "model-%05d.%s"%(i,ext)), process=False) for i in range(self.limits[0], self.limits[1])]
        
        if(self.timestamps is None or len(self.timestamps)!=len(self.meshes)):
            print("Wrong timestamps, default to a 50fps sequence")
            self.timestamps = torch.linspace(0,(self.limits[1]-self.limits[0])/50,steps=self.limits[1]-self.limits[0]).to(self.device) 
        
    def mesh_data(self):
        """Access the meshes representing the sequence
        Returns:
            list: A list of trimesh.mesh
        """

        return self.meshes

    def duration(self):
        """Returns the duration of the sequence in seconds"""
        return self.timestamps[-1].item()

    def number_of_frames(self):
        return self.limits[1]-self.limits[0]

    def frame_data(self, index, interpol_mode=0, unit="s", nopad=False):
        """Access a certain frame r set of frames of the sequence

        Args:
            index (int or float): timestamps in second or indexes of the frame to access
            interpol_mode (int, optional): _description_Interpolation stategy for inexact timestamps. Defaults to 0.
            unit (str, optional): unit of idx "s" for seconds "i" for integer indexes. Defaults to "s". 
            Requesting index in seconds for sequences with missing frames won't work
            nopad (bool, optional): If nopad is True, requesting indexes out of the sequence will raise an exception . Defaults to False.

        Raises:
            NotImplementedError: _description_
            IndexError: _description_

        Returns:
            (dict): pointcloud data
        """
        meshes = self.meshes

        if(unit == "s"):
            frame_id = self.framerate * \
                torch.tensor(index, dtype=torch.float32)
            if interpol_mode == 0:
                frame_id = frame_id.int()
            else:
                raise NotImplementedError(
                    "Interpolation mode not implemented yet")
        else:
            frame_id = torch.tensor(index, dtype=torch.int32)

        if (frame_id >= self.number_of_frames()).any():
            if(nopad):
                raise IndexError("Not enough frames")
            frame_id[frame_id >= self.number_of_frames()] = (
                self.number_of_frames()-1)

        data = {"meshes": [], "ind": [],"timestamps":[]}
        for fid in frame_id:
            data["meshes"].append(meshes[fid])
            data["ind"].append(fid)
            data["timestamps"].append(self.timestamps[fid])

        
        return data

    def save(self, new_folder):
        """Save the sequence at the location given by new_file

        Args:
            new_folder (str): Path where the data will be written
        """

        makepath(new_folder)
        with open(os.path.join(new_folder, "origin_folder.txt"), "w+") as f:
            f.write(self.origin_folder)

        torch.save(self.limits, os.path.join(
            new_folder, "limits.pt"))


    def get_data(self):
        return self.meshes,self.timestamps

