import torch
from torch.utils.data import Dataset
from torch.nn.functional import pad
import random
                 
class AmassDataset(Dataset):

    def __init__(self, smpl_sequences, configuration, body_model=None, dtype=torch.float32, labels2num=None):
        """Create a dataset encapsulating the sequences of smpl_sequences

        Args:
            smpl_sequences (list(SMPL_Sequence)): The data represented as a list of sequence object
            configuration (dict): Provides setup information
            body_model : A body_model to convert smpl data to mesh data
        """

        self.max_duration = configuration["max_duration"]
        self.min_duration = configuration["min_duration"]
        self.body_model = body_model
       
        self.dtype = dtype
       
        self.smpl_dataset = [s.cast(self.dtype).to("cpu")
                             for s in smpl_sequences]

        self.max_fps = int(max([s.framerate() for s in smpl_sequences]))
        self.max_frames = int(self.max_fps*self.max_duration)+1
  
        self.labels2num = labels2num
        self.num2labels = {labels2num[l]: l for l in labels2num.keys()}

        self.precompute = configuration["precompute"] # precompute should preferably be turned off, 
                                                    # inefficient implementation
        
    def __len__(self):
        return len(self.smpl_dataset)

    def __getitem__(self, idx):
        """Access the dataset

        Args:
            idx (int): idx of the element in the dataset

        Returns:
            dict: dict containing the smpl information and the timestamps
        """

        fps = self.smpl_dataset[idx].framerate()
        nof = self.smpl_dataset[idx].number_of_frames()

        if(nof < self.max_duration*fps):
            start = 0
            end = random.randint(int(fps*self.min_duration),
                                 nof)
        else:
            start = random.randint(0, int(nof-self.max_duration*fps))
            end = start +\
                random.randint(int(fps*self.min_duration),
                               int(self.max_duration*fps))

        return self.get_dict(idx, start, end)

    def get_dict(self, idx, start, end):

        smpl_data = self.smpl_dataset[idx].smpl_data()
       
        selected_data = dict()
        padding_length = self.max_frames - (end-start)

        padding = [0, 0, 0, padding_length]
        selected_data["poses"] = pad(
            smpl_data["poses"][start:end, :], padding)
        selected_data["trans"] = pad(
            smpl_data["trans"][start:end, :], padding)
        selected_data["betas"] = smpl_data["betas"]
        selected_data["mocap_framerate"] = smpl_data["mocap_framerate"].type(torch.int64)
        
        if self.precompute:
            mesh_data = self.smpl_dataset[idx].mesh_data()
            selected_data["mesh_data"] = pad(
            mesh_data[start:end], [0, 0, 0,0,0,padding_length])
          
        timestamp = (torch.linspace(
            0, (end-start)/smpl_data["mocap_framerate"], (end-start), dtype=self.dtype))/(self.max_duration)

        selected_data["timestamp"] = pad(
            timestamp, [0, padding_length], value=-1)
        selected_data["nof"] = torch.tensor(
            [(end-start)], dtype=torch.int64)
        selected_data["interval"] = torch.tensor(
            [start, end], dtype=torch.int64)

        selected_data["labels"] = pad(
            smpl_data["labels"][start:end, :], padding)

        return selected_data
