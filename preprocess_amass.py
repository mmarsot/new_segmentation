import json 
import argparse
import torch
import numpy as np
from human_body_prior.tools.omni_tools import makepath
import tqdm
from utils.file_management import list_files
import utils.representation as arepr

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("config", nargs="?",
                        help="path to a configuration file, defaults to config.json", default="config.json")
    parser.add_argument("--amass_train_folder", nargs="?")
    parser.add_argument("--amass_vald_folder", nargs="?")
    args = parser.parse_args()
    
    configuration = json.load(open(args.config))
    configuration["dtype"] = torch.float32
    configuration["device"]="cpu"
    
    makepath(configuration["training"]["folder"])
    makepath(configuration["validation"]["folder"])
    ### preprocessing by removing hand joints, dmpls  and converting to right angle representation
    train_files = list_files(args.amass_train_folder, pattern="poses.npz")
    validation_files = list_files(args.amass_vald_folder, pattern="poses.npz")

    for f in tqdm.tqdm(train_files):

        with np.load(f) as data:
            data = dict(data)
            data["poses"] = data["poses"][:, :66]
            data["poses"] = arepr.convertPose(torch.tensor(data["poses"]),arepr.AA,configuration["dataset"]["joint_repr"]).numpy()
            makepath(f.replace(args.amass_train_folder, configuration["training"]["folder"]), isfile=True)
            np.savez(f.replace(args.amass_train_folder, configuration["training"]["folder"]), **data)
    
    for f in tqdm.tqdm(validation_files):

        with np.load(f) as data:
            data = dict(data)
            data["poses"] = data["poses"][:, :66]
            data["poses"] = arepr.convertPose(torch.tensor(data["poses"]),arepr.AA,configuration["dataset"]["joint_repr"]).numpy()
            makepath(f.replace(args.amass_vald_folder, configuration["validation"]["folder"]), isfile=True)
            np.savez(f.replace(args.amass_vald_folder, configuration["validation"]["folder"]), **data)
    
