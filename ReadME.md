# Representing motion as a sequence of latent primitives

This is the code of the Arxiv publication Representing motion as a sequence of latent primitives, a flexible approach for human motion modelling

If you use it please cite the folliwing : 

```
@misc{marsot2022representing,
      title={Representing motion as a sequence of latent primitives, a flexible approach for human motion modelling}, 
      author={Mathieu Marsot and Stefanie Wuhrer and Jean-Sebastien Franco and Anne Hélène Olivier},
      year={2022},
      eprint={2206.13142},
      archivePrefix={arXiv},
      primaryClass={cs.CV}
}
```
# Data

This project uses the AMASS dataset from the Max Planck Institute. 
The data is available at https://amass.is.tue.mpg.de/

Please, also download the extended SMPL-H body model available at https://mano.is.tue.mpg.de

AMASS is composed of multiple smaller SUBSETS (ACCAD, CMU, KIT, ...), 
each containing MOTION.npz files performed by different SUBJECTS.

To correctly preprocess the data, your data folder structure should look like : 
```
DATA_FOLDER
    |
    +--train_split/
    |   +--SUBSET
    |       +--SUBJECT
    |           +--MOTION.npz
    |
    +--vald_split/
        +--SUBSET
            +--SUBJECT
                +--MOTION.npz
```
The split used to train the pretrained model is :

Training subsets: ACCAD;CNRS;GRAB;MPI_HDM05;BioMotionLab_NTroje;DanceDB;HUMAN4D;
MPI_Limits;BMLhandball;DFaust_67;HumanEva;TotalCapture;EKUT;KIT;
Transitions_mocap;CMU;Eyes_Japan_Dataset;MoSh

Validation subsets: SFU;SOMA;SSM_Synced;TCD_handMocap

# Requirements

The code using python 3.7 on Ubuntu 18.04 with 64GB of RAM and a Nvidia RTX2080Ti with 12GB memory. 
The python requirements are :

- human-body-prior 2.1.2
- pytorch 1.7
- pytorch3d 0.6.2
- torchgeometry 0.1.2
- trimesh
- open3d (only for visualization purposes)
- pyrender (for markerless completion visualization)
- git+https://github.com/nghorbani/body_visualizer (for markerless completion visualization)

Once the dependencies are installed, three minor changes are to be implemented in the human_body_prior package :

In human_body_prior/src/human_body_prior/body_model.py

Add a pose2rot bool argument to forward()

around line 178: 
```python
    def forward(self, root_orient=None, pose_body=None, pose_hand=None, 
        pose_jaw=None, pose_eye=None, betas=None,
        trans=None, dmpls=None, expression=None, 
        v_template =None, joints=None, v_shaped=None, 
        return_dict=False, pose2rot=True,  **kwargs):
```

Add a pose2rot argument to lbs()

around line 245: 
```python
    verts, Jtr = lbs(betas=shape_components, pose=full_pose, v_template=v_template,
        shapedirs=shapedirs, posedirs=self.posedirs,
        J_regressor=self.J_regressor, parents=self.kintree_table[0].long(),
        lbs_weights=self.weights, joints=joints, v_shaped=v_shaped,
        dtype=self.dtype, pose2rot=pose2rot)
```
In human_body_prior/src/lbs.py 

Add a reshape to the pose tensor

around line 228 :
```python 
    else:
        pose = pose.reshape(batch_size,-1,9)
        pose_feature = pose[:, 1:].view(batch_size, -1, 3, 3) - ident
        rot_mats = pose.view(batch_size, -1, 3, 3)
```

# Data preprocessing

Once the data is downloaded and the requirements installed run : 

```
python preprocess_amass.py configurations/config_parametric_256_8.json --amass_train_folder DATA_FOLDER/train_split/ --amass_vald_folder DATA_FOLDER/vald_split/
```

At this point, if you used the setup.sh script, your project structure should look like : 
```
ROOT_FOLDER
    +--sequence_of_latent_primitives/
    +--human_body_prior/
    +--body_models/
    |   +--smplh/
    +--AMASSnhpp
        +--AMASStrain/
        +--AMASSvald/
```
# Training 

To train the model, run : 

```
python train.py configurations/config_parametric_256_8.json --pointcloud_config configurations/config_pointcloud_256_8.json --pointcloud_start 1000
```

This will train the model on the parametric representation for 1000epochs (~2days). Then freeze the model and train the pointcloud encoder for 1000epochs (~1week).
For the arxiv results, the pointcloud encoder was only trained for 100epochs (~1day).

# Markerless completion

Given a sequence of pointclouds in a format compatible with trimesh.load() and associated timestamps stored as a one dimensional pytorch tensor in timestamps.pt.

```
SEQ_FOLDER
    +--model-00000.obj
    +--model-00001.obj
    +--model-00002.obj
    +--timestamps.pt
```
You can perform markerless motion capture by running : 

```
python markerless_completion --config configurations/config_parametric_256_8.json --pointcloud_config configurations/config_pointcloud_256_8.json --seq_folder SEQ_FOLDER/
```
