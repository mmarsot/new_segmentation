import torch
import torch.nn.functional as F


def kl_divergence(mu1, logvar1, mu_target=torch.zeros(1), logvar_target=torch.zeros(1)):
    """Returns kl divergence between mu and logvar and a normal distribution.

    Args:
        mu1 (tensor): The means of the distribution
        logvar1 (tensor): The logarithm of the variance of the distribution
        gamma (float, optional): A scaling coefficient. Defaults to 0.1.

    Shape:
        mu1 : N x D with N the batch size
        logvar1 : N x D with N the batch size

    Returns:
        tensor: The averaged KL divergence over the entire batch
    """
    if(mu_target.device != mu1.device):
        mu_target = mu_target.to(mu1.device)
    if(logvar_target.device != logvar1.device):
        logvar_target = logvar_target.to(logvar1.device)

    return torch.mean((-0.5*(1+logvar1-logvar_target-((mu1-mu_target).pow(2)/logvar_target.exp())-(logvar1-logvar_target).exp())))
