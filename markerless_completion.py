import torch
from models import get_model
import argparse
from body_models.smpl_model import SMPLH
from normalizers.normalizer_smpl import SMPL_normalizer
from train import create_dataset
import numpy as np
import trimesh
import argparse
import json
import random
from data.pc_sequence import PC_Sequence
from pytorch3d.loss import chamfer_distance
import time
import pyrender
from body_visualizer.tools.vis_tools import colors
from body_visualizer.mesh.mesh_viewer import MeshViewer
import torch.nn.functional as F
import os
from human_body_prior.tools.omni_tools import makepath
from utils.evaluation_tools import chamfer_l1


def plot_opti(meshes, meshes_dense,
              body_model, mv):

    visu_mesh = []
    mesh_seq = meshes
    meshes_to_show=[]
    for i in range(0,meshes.shape[0],20):
        visu_mesh.append(trimesh.Trimesh(vertices=mesh_seq[i, :, :].detach().cpu(), faces=body_model.get_faces().cpu(),
                                             vertex_colors=np.tile(colors['grey'], (6890, 1)), process=False))
        meshes_to_show.append(meshes_dense[i])
        
    mv.viewer.render_lock.acquire()
    mv.set_static_meshes([*meshes_to_show, *visu_mesh])
    mv.viewer.render_lock.release()

    time.sleep(1)

def motion_completion(model, pc_seq, configuration,timestamps, vert_per_frame=100000, lr=0.1, mv=None):

    gt_meshes,gt_timestamps = pc_seq.get_data()

    random.seed(0)
    minv = min([g.vertices.shape[0] for g in gt_meshes])
    samples = [random.sample(range(g.vertices.shape[0]), vert_per_frame if vert_per_frame <
                             minv else minv) for g in gt_meshes]

    gt_vertices = [torch.tensor(g.vertices, dtype=configuration["dtype"])[s, :].to(
        configuration["device"]) for g, s in zip(gt_meshes, samples)]
    gt_dense_data = torch.stack(gt_vertices)

    data_aligned_dense = gt_dense_data
    normalized_gt_timestamps = (gt_timestamps/dataset.max_duration).reshape(1, -1)
    normalized_timestamps= (timestamps/dataset.max_duration).reshape(1, -1)
    
    if model.pointcloud_encoding:
        
        # forward pass through the pointcloud encoder
        model.pointcloud_encoder.set_gt_input(
            data_aligned_dense[None,...])
        
       
        model.pointcloud_encoder.sampled_data["timestamp"] = normalized_gt_timestamps.to(
            data_aligned_dense.device)
        intermediate_params = {"sampled_data": model.pointcloud_encoder.sampled_data}
        model.pointcloud_encoder.encode(intermediate_params)
        
        
        init_latents = intermediate_params["latent_primitives"].detach().requires_grad_(True)
        init_shape = torch.zeros(
            (1, 8), device=init_latents.device).requires_grad_(True)
    else:
        init_latents = torch.zeros(
            (1, model.encoder.num_latents, model.encoder.latent_seg_dim), device=model.device, requires_grad=True)
        init_shape = torch.zeros(
            (1, 8), device=init_latents.device).requires_grad_(True)
        
    init_latents_cp = init_latents.detach().clone()
   
    model.pretrain(False)
    model.training = False

    optim = torch.optim.Adam(
        [init_shape,init_latents], lr=lr)
   
    i = 0
    cpt = 0
    loss_prev = 1000
    
    pts_dense = data_aligned_dense.cpu()
    sm_dense = trimesh.creation.uv_sphere(radius=0.01)
    sm_dense.visual.vertex_colors = [1.0, 0.0, 0.0]
    tfs_dense = np.tile(
        np.eye(4), (pts_dense.shape[0], pts_dense.shape[1], 1, 1))
    tfs_dense[:, :, :3, 3] = pts_dense
    meshes_dense = [pyrender.Mesh.from_trimesh(
        sm_dense, poses=tfs_dense[i]) for i in range(len(tfs_dense))]


    # Optimization loop 
    while i < 500 and cpt < 10:
        
        variables = {"latent_primitives": init_latents,
                     "dual": model.pointcloud_encoding}

        res = model.decoder.forward(
            normalized_gt_timestamps, init_shape, variables)
        
        meshes = model.body_model(
            model.normalizer.unnormalize({"poses": res[..., :-3].reshape(res.shape[1], -1),
                                          "trans": res[..., -3:].reshape(res.shape[1], -1),
                                          "betas": init_shape.repeat(res.shape[1], 1),
                                          "dmpls": None}),
            angle_repr=model.joint_repr,
            joints=model.joints,
            trans=True,
            pose2rot=False)
        
        loss_verts_dense = chamfer_distance(
            data_aligned_dense,
            meshes,
            point_reduction="mean",
            batch_reduction="mean")[0]

        l_reg = F.mse_loss(init_latents, init_latents_cp)
        loss=loss_verts_dense+0.1*l_reg
       
        if(loss.item() < loss_prev):
            loss_prev = loss.item()
            cpt = 0
        elif i > 100:
            cpt = cpt+1

        optim.zero_grad()
        loss.backward()
        optim.step()

        print("Ldense %.06f Lreg %.06f Cpt %03d" %
              (loss_verts_dense.item(), l_reg.item(), cpt), end='\r')
       
        if ((i < 100 and i % 100 == 0) or (i >= 100 and i % 100 == 0)) and (mv is not None):
            plot_opti(meshes, meshes_dense,
                      model.body_model, mv)
            
        i = i+1
    print()
    print("Final chamfer distance :" + str(int(chamfer_l1(meshes, gt_dense_data).detach().cpu().item()*1000)) + "mm")
    # Before returning forward pass with the desired timestamp, not the ground truth ones
    res =model.decoder.forward(
        normalized_timestamps, init_shape, variables)

    meshes = model.body_model(
        model.normalizer.unnormalize({"poses": res[..., :-3].reshape(res.shape[1], -1),
                                        "trans": res[..., -3:].reshape(res.shape[1], -1),
                                        "betas": init_shape.repeat(res.shape[1], 1),
                                        "dmpls": None}),
        angle_repr=model.joint_repr,
        joints=model.joints,
        trans=True,
        pose2rot=False) 
    
    return meshes, data_aligned_dense

if __name__ =="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str,
                        help="path to a configuration file", default="")
    parser.add_argument("--pointcloud_config", type=str,
                        help="path to a configuration file", default="")
    parser.add_argument("--seq_folder", type=str,
                        help="path to the PC files", default="")
    parser.add_argument("--out_fps", type=int,
                        help="desired output fps", default=50)
    parser.add_argument("--start", type=int, default=0)
    parser.add_argument("--end", type=int, default=-1)
    parser.add_argument("--relative", type=bool, default=False)
    parser.add_argument("--folder", default='eval/')
    args = parser.parse_args()
   
    configuration = json.load(open(args.config))
    configuration["dtype"] = torch.float32

    smplh = SMPLH(configuration)
    cpu_faces = smplh.get_faces().cpu()
    normalizer = SMPL_normalizer(configuration)
    model_class = get_model(configuration)

    if(args.pointcloud_config != ""):
        pointcloud_config = json.load(open(args.pointcloud_config))
    else:
        pointcloud_config = None
    
    model = model_class(configuration, normalizer, smplh, configuration_pointcloud=pointcloud_config).to(
        configuration["device"])
    model.pretrain(False)

    if(pointcloud_config):
        model.load_state_dict(torch.load(
            pointcloud_config["experiment"]["out_file"]),strict=False)
    else:
        model.load_state_dict(torch.load(
            configuration["experiment"]["out_file"]),strict=False)
    model.eval()
    
    
    dataset, loader = create_dataset(configuration, "validation", shuffle=False)
    
    seq_folders = [args.seq_folder]
    seq_abbvs = ["seq"]
        
    mv = MeshViewer(width=1200,
                    height=1200, use_offscreen=False)
    
    for vert_per_frame in [100]:
        try: 
            errors= torch.load(os.path.join(
                configuration["experiment"]["folder"], "errors%d.pt"%vert_per_frame))
        except:
            errors = dict()
            
        for seq_folder, seq_abbv in zip(seq_folders, seq_abbvs):  
            
            errors[seq_abbv] = dict()
                
            seq = PC_Sequence(
                seq_folder,  start=args.start, end=args.end, relative=args.relative, link=False)

            gt_meshes,gt_timestamps = seq.get_data()
            
            skip=False
            for i,m in enumerate(gt_meshes):
                if m is None:
                    skip=True
            if(skip):
                print("Missing GT frame %s"%seq_abbv)
                continue
            gt_vertices = [torch.tensor(g.vertices, dtype=configuration["dtype"]).to(
                configuration["device"]) for g in gt_meshes]

            
            makepath(os.path.join(
                args.folder, seq_abbv, "output_dense_%d"%vert_per_frame))

                
            vertices, gt_dense = motion_completion(
                model, seq, configuration, 
                timestamps=torch.linspace(0,seq.duration(),int(seq.duration()*args.out_fps)+1,device=model.device), 
                vert_per_frame=vert_per_frame ,mv=mv)

            
            for i, v in enumerate(vertices):
                eq_mesh = trimesh.Trimesh(vertices=v.detach().cpu().numpy(
               ), faces=model.body_model.get_faces().detach().cpu().numpy())
                eq_mesh.export(os.path.join(
                   args.folder, seq_abbv, "output_dense_%d"%vert_per_frame, "mesh%d.obj"%i))
            

            torch.save(errors, args.folder, "errors%d.pt"%vert_per_frame)
        
    mv.viewer.close()
