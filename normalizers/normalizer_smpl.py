import torch


class SMPL_normalizer:

    def __init__(self, configuration):
        """
        Args:
            configuration (dict): configuration parameters for the normalization
            see config_template.json for more details
        """

        self.normalize_posesb = configuration["normalization"]["poses"]
        self.normalize_transb = configuration["normalization"]["trans"]
        self.normalize_betasb = configuration["normalization"]["betas"]

        self.stats = torch.load(
            configuration["normalization"]["normalization_file"])

        self.dtype = configuration["dtype"]
        self.device = configuration["device"]

        for key in self.stats.keys():
            self.stats[key] = (self.stats[key][0].type(self.dtype).to(
                self.device).detach(), self.stats[key][1].type(self.dtype).to(self.device).detach())

    def normalize(self, data):
        """
        Args:
            data (dict): SMPL input data

        Returns:
            dict: the normalized SMPL data
        """

        normalized_data = dict(data)
        if(self.normalize_posesb):
            # ugly : if the model uses less joints than the dataset, only take the first normalization
            # but in write normalization, all joints normalization are computed
            # so if the removed joints are not the last ones there will be inconsistencies

            normalized_data["poses"] = (
                normalized_data["poses"] - self.stats["poses"][0]) / self.stats["poses"][1]

        if(self.normalize_betasb):
            n_betas = normalized_data["betas"].shape[-1]
            normalized_data["betas"] = (
                normalized_data["betas"] - self.stats["betas"][0][:n_betas]) / self.stats["betas"][1][:n_betas]

        if(self.normalize_transb):
            normalized_data["trans"] = self.normalize_trans(normalized_data)

        return normalized_data

    def normalize_trans(self, data):

        trans_std = (
            data["trans"] - self.stats["trans"][0]) / (self.stats["trans"][1] - self.stats["trans"][0])

        return trans_std * (1-(-1)) + (-1)

    def unnormalize_trans(self, data):
        return ((data["trans"]+1)/2) * (
            self.stats["trans"][1] - self.stats["trans"][0]) + self.stats["trans"][0]

    def unnormalize(self, data):
        """
        Args:
            data (dict): SMPL input data

        Returns:
            dict: the normalized SMPL data
        """

        unnormalized_data = dict(data)
        if(self.normalize_posesb):
            unnormalized_data["poses"] = unnormalized_data["poses"] * \
                self.stats["poses"][1][:unnormalized_data["poses"].shape[-1]
                                       ] + self.stats["poses"][0][:unnormalized_data["poses"].shape[-1]]

        if(self.normalize_betasb):
            n_betas = unnormalized_data["betas"].shape[-1]
            unnormalized_data["betas"] = unnormalized_data["betas"] * \
                self.stats["betas"][1][:n_betas] + \
                self.stats["betas"][0][:n_betas]

        if(self.normalize_transb):
            unnormalized_data["trans"] = self.unnormalize_trans(
                unnormalized_data)

        return unnormalized_data
