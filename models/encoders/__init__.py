from . import segmentation_encoder


def get_encoder(configuration, body_model=None, normalizer=None):

    if(configuration["model"]["encoder"]["type"] == "pointcloudencoder"):
        encoder = segmentation_encoder.PointcloudEncoder(
            configuration["model"]["encoder"], body_model=body_model, normalizer=normalizer)
    elif (configuration["model"]["encoder"]["type"] == "encoder"):
        encoder = segmentation_encoder.ParametricEncoder(
            configuration["model"]["encoder"])

    return encoder.to(configuration["device"])
