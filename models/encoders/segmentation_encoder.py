import torch
import torch.nn as nn
import math
import numpy as np
from utils.preprocess import resample_meshes
from ..pointnet import PointNetfeat
from pytorch3d.structures import Meshes

class PositionalEncodingLayer(nn.Module):
    """ This module generate a classical transformer positional encoding 
    but it is based on the timestamps of word number"""
    
    def __init__(self, configuration):
        super(PositionalEncodingLayer, self).__init__()

        self.freqs = 2*3.1415 / \
            (10000**(torch.linspace(0, configuration["latent_dim"],
                                    configuration["latent_dim"]//2)/configuration["latent_dim"]))[None, ...]

    def forward(self, t):
        """Takes a series of BSxN timestamp as input, return a BSxNxD positional encoding
        BS, batch size, N : number of timestamp, D : model dimension

        Args:
            t (torch.tensor): BSxN tensor of normalized timestamps 

        Returns:
            torch.tensor : positional encoding withof dimension BSxNxD
        """
        self.freqs=self.freqs.to(t.device)
        penc = torch.cat([torch.cos(
            t[..., None]*self.freqs[None, ...].repeat(t.shape[0], t.shape[1], 1)), torch.sin(
            t[..., None]*self.freqs[None, ...].repeat(t.shape[0], t.shape[1], 1))], dim=-1)
     
        return penc


class ParametricEncoder(nn.Module):
    """This is the parametric segmentation encoder, given a input sequence of parametric representation, 
    it projects it to a series of latent primitives characterizing the motion
    """
    def __init__(self, configuration):
        super(ParametricEncoder, self).__init__()

        self.frame_dim = configuration["frame_dim"]     # dimension of a parametric frame (element) pose+translation
        self.pre_train = True
        self.nframes = configuration["nframes"]
        self.num_latents = configuration["nsegments"]
        self.latent_dim = configuration["latent_dim"]

        
        self.positional_encoding = PositionalEncodingLayer(configuration)
        
        encoder_layer_segment = nn.TransformerEncoderLayer(
            d_model=self.latent_dim, nhead=configuration["nhead"])
        decoder_layer_segment = nn.TransformerDecoderLayer(
            d_model=self.latent_dim, nhead=configuration["nhead"])
        self.encoder_segments = torch.nn.TransformerEncoder(
            encoder_layer_segment, num_layers=configuration["num_layers"])
        self.decoder_segments = nn.TransformerDecoder(decoder_layer=decoder_layer_segment,
                                                      num_layers=configuration["num_layers"])
        

        self.embedding = nn.Linear(
            self.frame_dim, self.latent_dim)

        self.enc_mean_segments = nn.Sequential(
            nn.Linear(self.latent_dim, self.latent_dim),
            nn.ReLU(),
            nn.Linear(self.latent_dim, self.latent_dim))
        self.enc_logvar_segments = nn.Sequential(
            nn.Linear(self.latent_dim, self.latent_dim),
            nn.ReLU(),
            nn.Linear(self.latent_dim, self.latent_dim))

        
        # sampled_data contains the input sequence (N elements), 
        # if self.sparse is True, sparse sampled data is used instead to simulate sparse input
        # self.sparse_N controls the sparsity of the input (uniform sampling)
        self.sparse = False
        self.sparse_N = 10
        self.sampled_data = dict()
        self.sparse_sampled_data = dict()

        # positional encoding used as input for the transformer decoder
        self.init_enc = self.get_p_enc(
            self.num_latents, self.latent_dim)
         

    def sample(self, intermediate_params, input_data, n,precomputed=False):
        """ Sampled n element of the input sequence for easier training

        Args:
            intermediate_params (dict): intermediate values dictionnary
            input_data (dict): input data representing the motion
            n (int): number of element to sample from the input data
        """
        
        # sample n elements from sequences with different lengths
       
        sampling = np.linspace(np.zeros((input_data["frame_repr"].shape[0],)),  (
            input_data["lengths"].reshape(-1)-1).cpu(), n, dtype=np.int64)
        sampling = torch.from_numpy(sampling.T).to(
            input_data["frame_repr"].device)

        self.sampled_data["frame_repr"] = torch.gather(
            input_data["frame_repr"],
            1,
            sampling[:, :, None].repeat(
                1, 1, input_data["frame_repr"].shape[-1])
        )
        self.sampled_data["timestamp"] = torch.gather(
            input_data["timestamp"], 1, sampling)

        if precomputed:
            self.sampled_data["mesh_data"] = torch.gather(
            input_data["mesh_data"],
            1,
            sampling[:, :, None,None].repeat(
                1, 1, input_data["mesh_data"].shape[-2],3)
            )   
        intermediate_params["input_data"] = input_data
        intermediate_params["sampled_data"] = self.sampled_data
        intermediate_params["precompute"] = precomputed
        self.sampled_data["betas"] = input_data["betas"][...]

        # if sparse sampling, sample a subset of the already sampled data 
        # (self.sparse_sampled_data used for encoding, self.sampled_data is used to compute losses)
        if(self.sparse):
            sparse_sampling = np.linspace(np.zeros((input_data["frame_repr"].shape[0],)),  (
                input_data["lengths"].reshape(-1)-1).cpu(), self.sparse_N, dtype=np.int64)
            sparse_sampling = torch.from_numpy(sparse_sampling.T).to(
                input_data["frame_repr"].device)

            self.sparse_sampled_data["frame_repr"] = torch.gather(
                input_data["frame_repr"],
                1,
                sparse_sampling[:, :, None].repeat(
                    1, 1, input_data["frame_repr"].shape[-1])
            )

            self.sparse_sampled_data["timestamp"] = torch.gather(
                input_data["timestamp"], 1, sparse_sampling)

            intermediate_params["sparse_sampled_data"] = self.sparse_sampled_data
            self.sparse_sampled_data["betas"] = input_data["betas"][...]
            
            if(precomputed):
                self.sparse_sampled_data["mesh_data"] = torch.gather(
                    input_data["mesh_data"],
                1,
                sparse_sampling[:, :, None,None].repeat(
                    1, 1, input_data["mesh_data"].shape[-2],3)
                )
        else:
            self.sparse_sampled_data = self.sampled_data
            
    def compute_primitives_features(self, intermediate_params):
        """Compute features which characterize the sequence of primitive distribution using the transformer

        Args:
            intermediate_params (dict): intermediate values dictionnary

        Returns:
            torch.tensor: tensor of size BSxMxD of features with M the number of primitives
        """
        
        bs = self.sparse_sampled_data["frame_repr"].shape[0]
        proj = self.embedding(self.sparse_sampled_data["frame_repr"])

        proj_t = proj+self.positional_encoding(
                    self.sparse_sampled_data["timestamp"])
        
        # pytorch transformer takes batch size on second dimension
        proj_t = proj_t.transpose(0, 1)
        input_context = self.encoder_segments(proj_t)
        init = self.init_enc.repeat(1, bs, 1).to(proj.device)

        primitive_features = self.decoder_segments(tgt=init, memory=input_context)
        primitive_features = primitive_features.transpose(0, 1)

        return primitive_features

    def encode(self, intermediate_params, eps_reparam=None):
        """ Encode an input motion stored in self.sparse_sampled_data to a sequence of primitives 
        stored in intermediate_params["latent_primitives"]

        Args:
            intermediate_params (dict): dictionnary used to store intermediate values and the output
            eps_reparam (_type_, optional): noise used to sample from primitive distribution, 
                                                if None, random gaussian noise
        """
        primitive_features = self.compute_primitives_features(intermediate_params)

        intermediate_params["mu_segments"] = self.enc_mean_segments(
            primitive_features)
        intermediate_params["logvar_segments"] = self.enc_logvar_segments(
            primitive_features)

        if(eps_reparam is not None):
            intermediate_params["eps_segments"] = eps_reparam
        else:
            intermediate_params["eps_segments"] = torch.randn_like(
                intermediate_params["logvar_segments"])
            
        intermediate_params["latent_primitives"] = self.reparameterize(
            intermediate_params["mu_segments"], intermediate_params["logvar_segments"], eps=intermediate_params["eps_segments"])


    def forward(self, input_data, intermediate_params, precomputed=False,eps_reparam=None):
        
        self.sample(intermediate_params, input_data,
                    self.nframes,precomputed=precomputed)
        
        self.encode(intermediate_params, eps_reparam=eps_reparam)
        
    def get_p_enc(self, N, D):
        
        p_enc = torch.zeros((N, D))
        for pos in range(N):
            for i in range(0, D, 2):
                p_enc[pos, i] = math.sin(pos / (10000 ** ((2 * i)/D)))
                p_enc[pos, i + 1] = math.cos(pos /
                                             (10000 ** ((2 * (i + 1))/D)))

        p_enc = p_enc.unsqueeze(1)

        return p_enc

    def get_n_params(self):
        model_parameters = filter(lambda p: p.requires_grad, self.parameters())
        params = sum([np.prod(p.size()) for p in model_parameters])
        return params
    
    def reparameterize(self, mu, logvar, eps=None):

        std = torch.exp(0.5*logvar)
        if eps is None:
            eps = torch.randn_like(std)

        return mu + eps*std

class PointcloudEncoder(ParametricEncoder):
    def __init__(self, configuration, body_model, normalizer):
        super(PointcloudEncoder, self).__init__(configuration)

        self.embedding = nn.Sequential(
            nn.Linear(1024+3, self.latent_dim),
            nn.ReLU(),
            nn.Linear(self.latent_dim, self.latent_dim))

        self.point_net_feat = PointNetfeat()
        self.body_model = body_model
        self.normalizer = normalizer
        self.joint_repr = configuration["joint_repr"]
        self.joints = configuration["joints"]
        self.precompute=False

    def compute_primitives_features(self, intermediate_params):

        bs, nof, N = self.sampled_data["normalized_resampled_meshes"].shape[0], self.sampled_data[
            "normalized_resampled_meshes"].shape[1], self.sampled_data["normalized_resampled_meshes"].shape[2]

        # changing the embedding with a PointNet
        pc_feat = self.point_net_feat(
            self.sampled_data["normalized_resampled_meshes"].reshape(bs*nof, N, 3).transpose(-1, -2))[0]
        pc_feat = pc_feat.reshape(bs, nof, -1)
        pc_trans = self.sampled_data["resampled_trans"][:, :, 0, :]
       
        proj = self.embedding(torch.cat((pc_feat, pc_trans), dim=-1))
        
        proj_t = proj+self.positional_encoding(
                    self.sampled_data["timestamp"])
        proj_t = proj_t.transpose(0, 1)
        
        input_context = self.encoder_segments(proj_t)
        init = self.init_enc.repeat(1, bs, 1).to(proj.device)

        primitive_features = self.decoder_segments(tgt=init, memory=input_context)
        primitive_features = primitive_features.transpose(0, 1)

        return primitive_features

    def sample(self, intermediate_params, input_data, n, p=100,precomputed=False):
        """For this encoder, the sampling requires to sample meshes, 
        meshes which are computed using parametric representation for lower disk usage
        or sampling direclty from precomputed meshes for faster training"""

        sampling = np.linspace(np.zeros((input_data["frame_repr"].shape[0],)),  (
            input_data["lengths"].reshape(-1)-1).cpu(), n, dtype=np.int64)
        sampling = torch.from_numpy(sampling.T).to(
            input_data["frame_repr"].device)

        self.sampled_data["frame_repr"] = torch.gather(
            input_data["frame_repr"],
            1,
            sampling[:, :, None].repeat(
                1, 1, input_data["frame_repr"].shape[-1])
        )

        self.sampled_data["timestamp"] = torch.gather(
            input_data["timestamp"], 1, sampling)

        bs, n_timestamps = self.sampled_data["timestamp"].shape[0], self.sampled_data["timestamp"].shape[1]

        self.sampled_data["betas"] = input_data["betas"][...]

        # for the pointcloud encoder, we need to randomly sample from SMPL meshes, increasing training time
        # can be accelerating by computing the mesh data for all AMASS and using it using the "precompute" bool
        if not self.precompute:
            mesh_vertices = self.body_model(
                self.normalizer.unnormalize({"poses": self.sampled_data["frame_repr"][..., :-3].reshape(bs*n_timestamps, -1),
                                            "trans": self.sampled_data["frame_repr"][..., -3:].reshape(bs*n_timestamps, -1),
                                            "betas": self.sampled_data["betas"][:, None, :].repeat(1, n_timestamps, 1).reshape(bs*n_timestamps, -1),
                                            "dmpls": None}),
                angle_repr=self.joint_repr,
                joints=self.joints,
                trans=True,
                pose2rot=False).reshape(-1, 6890, 3)
            faces = self.body_model.get_faces()[None, ...].repeat(bs*n_timestamps, 1, 1)
            meshes = Meshes(mesh_vertices, faces)
        else:
            meshes = input_data["meshes"].__getitem__(sampling)
            
        self.sampled_data["meshes_input"] = mesh_vertices
        self.sampled_data["resampled_meshes"] = resample_meshes(
            meshes,N=p).reshape(bs, n_timestamps, p, 3)

        self.sampled_data["resampled_trans"] = torch.mean(
            self.sampled_data["resampled_meshes"], dim=2, keepdim=True)

        self.sampled_data["normalized_resampled_meshes"] = self.sampled_data["resampled_meshes"] - \
            self.sampled_data["resampled_trans"]

        # mini = torch.min(self.sampled_data["normalized_resampled_meshes"].view(
        #     bs, n_timestamps, -1), dim=-1)[0][:, :, None, None]
        maxi = torch.max(torch.sqrt(torch.sum((self.sampled_data["normalized_resampled_meshes"]**2), dim=-1)),dim=-1)[0][:,:,None,None]
      
        self.sampled_data["normalized_resampled_meshes"] = (self.sampled_data["normalized_resampled_meshes"])/(
            maxi)

        self.sampled_data["resampled_trans"] = self.normalizer.normalize_trans(
            {"trans": self.sampled_data["resampled_trans"]})
        intermediate_params["sampled_data"] = self.sampled_data
        intermediate_params["sparsity"] = p

        self.sparse_sampled_data = self.sampled_data
    def set_gt_input(self, pointcloud_data):
        """Set input using real pointcloud data"""

        bs = pointcloud_data.shape[0]
        self.sampled_data["resampled_meshes"] = pointcloud_data

        self.sampled_data["resampled_trans"] = torch.mean(
            self.sampled_data["resampled_meshes"], dim=2, keepdim=True)
        self.sampled_data["normalized_resampled_meshes"] = self.sampled_data["resampled_meshes"] - \
            self.sampled_data["resampled_trans"]

        maxi = torch.max(torch.sqrt(torch.sum((self.sampled_data["normalized_resampled_meshes"]**2), dim=-1)),dim=-1)[0][:,:,None,None]
      
        self.sampled_data["normalized_resampled_meshes"] = (self.sampled_data["normalized_resampled_meshes"])/(
            maxi)

        self.sampled_data["resampled_trans"] = self.normalizer.normalize_trans(
            {"trans": self.sampled_data["resampled_trans"]})
        
