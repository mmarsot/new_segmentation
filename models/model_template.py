
from cv2 import DRAW_MATCHES_FLAGS_NOT_DRAW_SINGLE_POINTS
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from utils.error import InvalidShapeException

from utils.representation import convertPose

from models.encoders import get_encoder
from models.decoders import get_decoder
from utils.preprocess import apply_transfo, get_global_transfo


class Model(nn.Module):

    def __init__(self, configuration, normalizer, body_model=None, configuration_pointcloud=None):
        super(Model, self).__init__()

        self.load_properties(configuration)

        # encoder converts the input in a latent representation E(S) = z
        self.encoder = get_encoder(
            configuration, body_model=body_model, normalizer=normalizer)

        # implicit decoder, convert a latent representation, shape and time to a frame : D(z,B,t) = S(t)
        self.decoder = get_decoder(configuration)

        self.normalizer = normalizer
        self.body_model = body_model

        self.input = {"frame_repr": None, "timestamp": None, "lengths": None}
        self.intermediate_variables = {}
        self.output = None
        self.loss_details = {}
        self.loss = 0
        self.optimizer = None
        self.precomputed_meshes = configuration["dataset"]["precompute"]
        
        self.pointcloud_encoding = not (configuration_pointcloud is None)
        if(self.pointcloud_encoding):
            self.pointcloud_intermediate_variables = {}
            self.pointcloud_encoder = get_encoder(
                configuration_pointcloud, body_model=body_model, normalizer=normalizer)

    def forward(self, decoding_timestamp=None):

        self.encode()

        if(decoding_timestamp is None):
            self.intermediate_variables["decoder_sampled_data"] = self.intermediate_variables["sampled_data"]
            decoding_timestamp = self.intermediate_variables["sampled_data"]["timestamp"]
            if self.pointcloud_encoding:
                self.pointcloud_intermediate_variables["decoder_sampled_data"] = self.pointcloud_intermediate_variables["sampled_data"]
        self.output = self.decode(
            decoding_timestamp, self.intermediate_variables["sampled_data"]["betas"])

        return self.output

    def encode(self):

        self.encoder(self.input, self.intermediate_variables,precomputed=self.precomputed_meshes)

        if(self.pointcloud_encoding):
            self.pointcloud_encoder(
                self.input, self.pointcloud_intermediate_variables, eps_reparam=self.intermediate_variables["eps_segments"])

    def decode(self, timestamps, morphology, aligned=True):

        # modifies intermediates variables
        self.intermediate_variables["pointcloud"] = False
        out = self.decoder(
            timestamps, morphology, self.intermediate_variables)

        if(self.pointcloud_encoding):
            self.pointcloud_intermediate_variables["pointcloud"] = True
            self.decoder(
                timestamps, self.pointcloud_intermediate_variables["sampled_data"]["betas"],
                self.pointcloud_intermediate_variables)

        if aligned:
            return out
        else:
            out_unalign = apply_transfo(
                out[:, None, ...], self.transfo_0[:, None, ...],
                transfo_angle_repr=self.joint_repr,
                model_angle_repr=self.joint_repr,
                inv=True)

            return out_unalign[:, 0, ...]

    def compute_loss(self, loss_params):
        # updates self.loss and self.loss_details
        raise NotImplementedError("Please Implement this method")

    def add_pointcloud_encoder(self, configuration_pointcloud):
        self.pointcloud_encoding = True
        self.pointcloud_intermediate_variables = {}
        self.pointcloud_encoder = get_encoder(
            configuration_pointcloud, body_model=self.body_model, normalizer=self.normalizer)

    def load_properties(self, configuration):
        self.device = configuration["device"]
        self.dtype = configuration["dtype"]
        self.joint_repr = configuration["model"]["joint_repr"]
        self.joints = configuration["model"]["joints"]
        self.normalization_params = configuration["normalization"]

    def initialize_optimizer(self, configuration):
        self.optimizer = torch.optim.Adam(
            self.parameters(), lr=configuration["training"]['lr'])
        self.scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            self.optimizer, mode='min', factor=0.1, patience=20,
            threshold=0.0001, threshold_mode='rel', cooldown=50,
            min_lr=1e-7, eps=1e-08, verbose=True)

    def optimize_parameters(self):
        """
        Calculate gradients and update network weights.
        """
        # optimizing network and dumping the graph
        self.optimizer.zero_grad()
        self.loss.backward()
        
        nn.utils.clip_grad_value_(self.parameters(), clip_value=1.0)
        self.optimizer.step()

    def set_input(self, data, data_params):
        """Preprocess the data and set it as input for the forward pass. /!\ Can modify data dimension"""

        if(len(data["poses"].shape) != 3):
            print("Wrong data dimensions")
            raise InvalidShapeException

        if not data_params["preprocessed"]:
            data["poses"] = convertPose(
                data["poses"], data_params["joint_repr"], self.joint_repr)

        data["poses"] = data["poses"].reshape(
            data["poses"].shape[0], data["poses"].shape[1], - 1, self.joint_repr["dim"])
        data["poses"] = data["poses"][:, :, self.joints, :]
        data["poses"] = data["poses"].reshape(
            data["poses"].shape[0], data["poses"].shape[1], -1)

        if(not data_params["normalized"]):
            self.input = self.normalizer.normalize(data)
        else:
            self.input = data

        self.input["frame_repr"] = torch.cat(
            [self.input["poses"], self.input["trans"]], dim=-1)
        self.input["lengths"] = self.input["nof"]
        if(data_params["precompute"]):
            self.input["meshes"] = data["mesh_data"]
            
    def get_loss_details(self):
        return self.loss_details

    def save_networks(self, out_file):
        if(not "model" in out_file):
            print("Outfile should contain substring 'model'")
            raise ValueError

        torch.save(self.state_dict(), out_file)
        torch.save(self.optimizer.state_dict(),
                   out_file.replace("model", "optim"))

    def get_n_params(self):
        model_parameters = filter(lambda p: p.requires_grad, self.parameters())

        params = sum([np.prod(p.size()) for p in model_parameters])

        return params

    def pretrain(self, status):
        self.encoder.pre_train = status
        self.decoder.pre_train = status
