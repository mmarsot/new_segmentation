import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from utils.preprocess import apply_transfo
from .model_template import Model
from losses.losses import kl_divergence



class TemporalSegmentationModel(Model):

    def __init__(self, configuration, normalizer, body_model, configuration_pointcloud=None):
        super(TemporalSegmentationModel, self).__init__(
            configuration, normalizer, body_model, configuration_pointcloud=configuration_pointcloud)
        
        self.joints_to_use = np.array([
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
            11, 12, 13, 14, 15, 16, 17, 18, 19,
            20, 21, 22, 37
        ])
        self.num_latents = configuration["model"]["encoder"]["nsegments"]

    def compute_loss(self, epoch, loss_params):

        bs, n_timestamps = self.intermediate_variables["decoder_sampled_data"]["timestamp"].shape[:2]
        repeated_input = self.intermediate_variables["decoder_sampled_data"]["frame_repr"][:, None, ...].repeat(
            1, self.num_latents, 1, 1)

        repeated_masks = self.intermediate_variables["masks"].repeat(
            1, 1, 1, self.intermediate_variables["decoder_sampled_data"]["frame_repr"].shape[-1])

        repeated_transfo = self.intermediate_variables["global_transfo"][:, :, None, :].repeat(
            1, 1, n_timestamps, 1)
        
        # align the segments in canonical space using predicted transfo 
        aligned_input = apply_transfo(repeated_input, repeated_transfo,
                                      transfo_angle_repr=self.joint_repr, model_angle_repr=self.joint_repr, inv=True)

        l_pose = torch.mean(
            ((self.intermediate_variables["rec_seq"][..., :-3]-self.intermediate_variables["decoder_sampled_data"]["frame_repr"][..., :-3])**2))
        l_trans = torch.mean(
            ((self.intermediate_variables["rec_seq"][..., -3:]-self.intermediate_variables["decoder_sampled_data"]["frame_repr"][..., -3:])**2))

        l_seg_pose = torch.mean(torch.sum(
            ((self.intermediate_variables["rec_subseqs"][..., :-3]-aligned_input[..., :-3])**2)*repeated_masks[..., :-3].detach(), dim=1))
        l_seg_trans = torch.mean(torch.sum(
            ((self.intermediate_variables["rec_subseqs"][..., -3:]-aligned_input[..., -3:])**2)*repeated_masks[..., -3:].detach(), dim=1))
        
        kl_segments = kl_divergence(
            self.intermediate_variables["mu_segments"], self.intermediate_variables["logvar_segments"])
        

        if(self.pointcloud_encoding):
            loss_pointcloud_z = F.mse_loss(
                self.pointcloud_intermediate_variables["mu_segments"],
                self.intermediate_variables["mu_segments"].detach())+ F.mse_loss(self.pointcloud_intermediate_variables["logvar_segments"],
                self.intermediate_variables["logvar_segments"].detach())
            loss_pointcloud_segm = F.mse_loss(
                self.pointcloud_intermediate_variables["segm_params"], self.intermediate_variables["segm_params"].detach())
            loss_pointcloud_pose = torch.mean(
                ((self.pointcloud_intermediate_variables["rec_seq"][..., :-3]-self.intermediate_variables["decoder_sampled_data"]["frame_repr"][..., :-3].detach())**2))
            loss_pointcloud_trans = torch.mean(
                ((self.pointcloud_intermediate_variables["rec_seq"][..., -3:]-self.intermediate_variables["rec_seq"][..., -3:].detach())**2))
        else:
            loss_pointcloud_z = torch.tensor(
                0, device=repeated_input.device)
            loss_pointcloud_segm = torch.tensor(
                0, device=repeated_input.device)
            loss_pointcloud_pose = torch.tensor(
                0, device=repeated_input.device)
            loss_pointcloud_trans = torch.tensor(
                0, device=repeated_input.device)

        if(epoch > loss_params["mesh_start"]):
            weight = float(loss_params["mesh_weight"])
                         
            if(not self.pointcloud_encoding):
              
                meshes_output = self.body_model(
                    self.normalizer.unnormalize({"poses": self.intermediate_variables["rec_seq"][..., :-3].reshape(bs*n_timestamps, -1),
                                                "trans": self.intermediate_variables["rec_seq"][..., -3:].reshape(bs*n_timestamps, -1),
                                                "betas": self.intermediate_variables["decoder_sampled_data"]["betas"][:, None, :].repeat(1, n_timestamps, 1).reshape(bs*n_timestamps, -1),
                                                "dmpls": None}),
                    angle_repr=self.joint_repr,
                    joints=self.joints,
                    trans=True,
                    pose2rot=False,
                    Jtr=False)
            else:
            
                meshes_output = self.body_model(
                self.normalizer.unnormalize({"poses": self.pointcloud_intermediate_variables["rec_seq"][..., :-3].reshape(bs*n_timestamps, -1),
                                                "trans": self.pointcloud_intermediate_variables["rec_seq"][..., -3:].reshape(bs*n_timestamps, -1),
                                                "betas": self.intermediate_variables["decoder_sampled_data"]["betas"][:, None, :].repeat(1, n_timestamps, 1).reshape(bs*n_timestamps, -1),
                                                "dmpls": None}),
                angle_repr=self.joint_repr,
                joints=self.joints,
                trans=True,
                pose2rot=False,
                Jtr=False)

            if(self.intermediate_variables["precompute"]):
                meshes_input = self.intermediate_variables["decoder_sampled_data"]["mesh_data"].reshape(bs*n_timestamps, 6890,3)
            elif self.pointcloud_encoding:
                meshes_input = self.pointcloud_intermediate_variables["decoder_sampled_data"]["meshes_input"]
            else:
                meshes_input = self.body_model(
                    self.normalizer.unnormalize({"poses": self.intermediate_variables["decoder_sampled_data"]["frame_repr"][..., :-3].reshape(bs*n_timestamps, -1),
                                                    "trans": self.intermediate_variables["decoder_sampled_data"]["frame_repr"][..., -3:].reshape(bs*n_timestamps, -1),
                                                    "betas": self.intermediate_variables["decoder_sampled_data"]["betas"][:, None, :].repeat(1, n_timestamps, 1).reshape(bs*n_timestamps, -1),
                                                    "dmpls": None}),
                    angle_repr=self.joint_repr,
                    joints=self.joints,
                    trans=True,
                    pose2rot=False,
                    Jtr=False)

            l_mesh = torch.mean(
                (meshes_input-meshes_output)**2)
            error_mesh = torch.mean(
                torch.sqrt(torch.sum((meshes_input-meshes_output)**2, dim=-1)))
        else:
            
            weight = 1.0
            error_mesh = torch.tensor(
                0, device=repeated_input.device)
            l_mesh = torch.tensor(
                0, device=repeated_input.device)

        error_pose = torch.mean(
            torch.abs(self.intermediate_variables["rec_seq"][..., :-3]-self.intermediate_variables["decoder_sampled_data"]["frame_repr"][..., :-3])).cpu().detach()

      
        if(epoch > loss_params["deactivate_duration"]):
            l_durations = torch.tensor(
                0, device=repeated_input.device)
           
        else:
            l_durations = torch.mean(
                (self.intermediate_variables["segm_params"][..., 1]-1/self.num_latents)**2)

        if(not self.pointcloud_encoding):
            
            self.loss = loss_params["kl_weight"] * kl_segments +\
                l_pose+l_trans+l_seg_pose+l_seg_trans +\
                loss_params["mask_reg_weight"] * l_durations + weight*l_mesh 
                    

        else:
            print(self.pointcloud_intermediate_variables["rec_seq"][0,..., -3:])
            self.loss = loss_pointcloud_segm+loss_pointcloud_z + \
                loss_pointcloud_pose+loss_pointcloud_trans + l_mesh

        self.loss_details = {
            "l_trans": l_trans.detach().cpu(),
            "l_pose": l_pose.detach().cpu(),
            "l_seg_pose": l_seg_pose.detach().cpu(),
            "l_seg_trans": l_seg_trans.detach().cpu(),
            "kl_segments": kl_segments.detach().cpu(),
            "l_mesh": l_mesh.detach().cpu(),
            "l_durations": l_durations.detach().cpu(),
            "loss_pointcloud_pose": loss_pointcloud_pose.detach().cpu(),
            "loss_pointcloud_trans": loss_pointcloud_trans.detach().cpu(),
            "loss_pointcloud_segm": loss_pointcloud_segm.detach().cpu(),
            "loss_pointcloud_z": loss_pointcloud_z.detach().cpu(),
            "error_mesh": error_mesh.detach().cpu(),
            "error_pose": error_pose.detach().cpu(),
            "weighted total": self.loss.detach().cpu()
        }
