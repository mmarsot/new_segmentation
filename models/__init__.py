from .segmentation_model import *


def get_model(configuration):

    if configuration["model"]["type"] == "temporalsegmentation":
        return TemporalSegmentationModel
    
