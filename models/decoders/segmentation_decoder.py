import torch
import torch.nn as nn
import math
import numpy as np
from utils.preprocess import apply_transfo

class SegmentationDecoder(nn.Module):
    def __init__(self, configuration, joint_repr):
        super(SegmentationDecoder, self).__init__()

        self.frame_dim = configuration["frame_dim"]
        self.morpho_dim = configuration["latent_morpho_dim"]
        self.pre_train = True
        
        self.num_primitives = configuration["nsegments"]
        # intermediate representation dimension
        self.latent_dim = configuration["latent_dim"]
        self.feat_time = 8
        self.joint_repr = joint_repr
        self.slope = configuration["masking_slope"]
        self.masking = configuration["masking"]
        
        hidden_dim = configuration["hidden_dim"]
        
        self.decoder = nn.Sequential(
            nn.Linear(self.latent_dim+self.morpho_dim +
                      2*self.feat_time, hidden_dim),
            nn.ReLU(),
            nn.LayerNorm(hidden_dim),
            nn.Linear(hidden_dim, hidden_dim),
            nn.ReLU(),
            nn.LayerNorm(hidden_dim),
            nn.Linear(hidden_dim, self.frame_dim))
        
        encoder_layer_segment = nn.TransformerEncoderLayer(
            d_model=self.latent_dim, nhead=configuration["nhead"])
        self.encoder_segments = torch.nn.TransformerEncoder(
            encoder_layer_segment, num_layers=configuration["num_layers"])

        self.segmentation_adjust = nn.Sequential(
            nn.Linear(self.latent_dim+self.morpho_dim,
                      self.latent_dim),
            nn.ReLU(),
            nn.Linear(self.latent_dim, 1),
            nn.Sigmoid())

        self.transfo_adjust = nn.Sequential(
            nn.Linear(self.latent_dim+self.morpho_dim,
                      self.latent_dim),
            nn.ReLU(),
            nn.Linear(self.latent_dim, joint_repr["dim"]+3))

        self.penc = self.get_p_enc(self.num_primitives, self.latent_dim)
        self.learn_segm = configuration["learn_segm"]

    def masked_average(self, subseqs, transfos, masks, eps=1e-7):
        rec = torch.sum(apply_transfo(subseqs,
                                      transfos,
                                      transfo_angle_repr=self.joint_repr,
                                      model_angle_repr=self.joint_repr,
                                      inv=False)*masks, dim=1) / (torch.sum(masks, dim=1)+eps)
        return rec

    def get_n_params(self):
        model_parameters = filter(lambda p: p.requires_grad, self.parameters())
        params = sum([np.prod(p.size()) for p in model_parameters])
        return params
    
    def create_masks(self, segm_params, t, eps=1e-7):

        if self.masking == "gaussian":
            x = (t-(segm_params[..., 0:1]+segm_params[...,
                                                      1:2]/2))/(segm_params[..., 1:2]/2+eps)
            return torch.exp(-self.slope*x**2)
        elif self.masking == "sigmoid":
            x = (t-segm_params[..., 0:1])/(segm_params[..., 1:2]+eps)
            return nn.Sigmoid()(self.slope*(x)) - nn.Sigmoid()(self.slope*(x-1))
        elif self.masking == "binary": # non differentiable !
            return torch.logical_and((t >= segm_params[..., 0:1]), (t <= (
                segm_params[..., 0:1]+segm_params[..., 1:2]))).int().float()
            
   
    def compute_segments_transfo(self, intermediate_params, morphology, use_gt_transfo, offset=True):
        
        # compute features aware of the entire segment sequence to deduce relative transformations and durations
        Y = intermediate_params["latent_primitives"].transpose(
            0, 1)+self.penc.repeat(
            1, intermediate_params["latent_primitives"].shape[0], 1).to(intermediate_params["latent_primitives"].device)
        intermediate_params["decoder_features"] = self.encoder_segments(
            Y).transpose(0, 1)

        X = torch.cat(
            (intermediate_params["decoder_features"], morphology[:, None, :self.morpho_dim].repeat(
                1, self.num_primitives,  1)), dim=-1)
        
        if(self.learn_segm):
            intermediate_params["segm_params"] = self.segmentation_adjust(
                X)
        else:
            intermediate_params["segm_params"] = torch.ones(
                (X.shape[0], X.shape[1], 1), device=X.device, requires_grad=False)/self.num_primitives

        intermediate_params["segm_params"] = torch.cat([torch.cat(
            [torch.zeros_like(intermediate_params["segm_params"][..., 0:1, :]), torch.cumsum(
                intermediate_params["segm_params"][..., :-1, :], dim=1)], dim=1), intermediate_params["segm_params"]], dim=-1)

        intermediate_params["global_transfo"] = self.transfo_adjust(X)


    def forward(self, timestamps, morphology, intermediate_params, use_gt_transfo=True):
 
        self.compute_segments_transfo(
            intermediate_params, morphology, use_gt_transfo, offset=True)

        bs, n_timestamps = timestamps.shape[0], timestamps.shape[1]

        repeated_morphology = morphology[:, None, None, :self.morpho_dim].repeat(
            1, self.num_primitives, n_timestamps, 1)
        repeated_timestamps = timestamps[:, None, :, None].repeat(
            1, self.num_primitives, 1, 1)

        repeated_latents = intermediate_params["latent_primitives"][:, :, None, :].repeat(
            1, 1, n_timestamps, 1)
        repeated_seg_params = intermediate_params["segm_params"][:, :, None, :].repeat(
            1, 1, n_timestamps, 1)
        
        warped_timestamps = nn.ReLU()(
            repeated_timestamps-repeated_seg_params[..., 0: 1])

        coefs = self.get_t_enc()[None, None, None, :].repeat(
            bs, self.num_primitives, n_timestamps, 1).to(warped_timestamps.device)

        higher_dim_timestamp = torch.cat([torch.sin(
            coefs*warped_timestamps.repeat(1, 1, 1, self.feat_time)), torch.cos(
            coefs*warped_timestamps.repeat(1, 1, 1, self.feat_time))], dim=-1)
        
        repeated_transfo = intermediate_params["global_transfo"][:, :, None, :].repeat(
            1, 1, n_timestamps, 1)

        intermediate_params["rec_subseqs"] = self.decoder(
            torch.cat([repeated_latents, repeated_morphology, higher_dim_timestamp], dim=-1))

        intermediate_params["masks"] = self.create_masks(
            repeated_seg_params, repeated_timestamps)
        intermediate_params["rec_seq"] = self.masked_average(
            intermediate_params["rec_subseqs"], repeated_transfo, intermediate_params["masks"])

        return intermediate_params["rec_seq"]

    def get_p_enc(self, N, D):
        p_enc = torch.zeros((N, D))
        for pos in range(N):
            for i in range(0, D, 2):
                p_enc[pos, i] = math.sin(pos / (10000 ** ((2 * i)/D)))
                p_enc[pos, i + 1] = math.cos(pos /
                                             (10000 ** ((2 * (i + 1))/D)))

        p_enc = p_enc.unsqueeze(1)

        return p_enc

    def get_t_enc(self):
        return (2**(torch.arange(0, self.feat_time))*3.1415)


