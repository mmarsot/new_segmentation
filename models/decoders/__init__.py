from . import segmentation_decoder


def get_decoder(configuration):

    if(configuration["model"]["decoder"]["type"] == "decoder"):
        decoder = segmentation_decoder.SegmentationDecoder(
            configuration["model"]["decoder"], configuration["model"]["joint_repr"])

    return decoder.to(configuration["device"])
