import torch

class InvalidShapeException(Exception):
    pass

def checkTensor(*args): 
    for i, arg in enumerate(args):
        if not torch.is_tensor(arg):
            raise TypeError("Expected Tensor, got {} (arg nb {})".format(type(arg),i))

def checkNumDims(*args):
    
    ref_num_dim = len(args[0].shape)

    for arg in args[1:]:
        if(len(arg.shape)!= ref_num_dim):
            raise InvalidShapeException("Tensors should have same number of dims")