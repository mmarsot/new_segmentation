
import torch
import utils.representation as arepr
from pytorch3d.ops import knn_points, knn_gather, sample_points_from_meshes



def resample_meshes(meshes, N):

    
    resampled_meshes = sample_points_from_meshes(meshes, num_samples=N)

    return resampled_meshes


def get_global_transfo(data, t0, lengths, angle_repr=arepr.SIX, offset=True, mode="simple", only_z=False):

    if(mode == "interpol"):
        _, idx, _ = knn_points(
            t0, data[:, :, -1:], lengths1=None, lengths2=lengths, K=2, return_sorted=False)
        nn = knn_gather(data, idx)

        low, _ = nn[..., -1:].min(dim=-2)
        up, _ = nn[..., -1:].max(dim=-2)
        tinterp = (t0-low)/(up-low)
        tinterp[t0 < low] = 0.0
        tinterp[t0 > up] = 1.0

    else:

        _, idx, _ = knn_points(
            t0, data[:, :, -1:], lengths1=None, lengths2=lengths, K=1, return_sorted=False)
        nn = knn_gather(data, idx).squeeze(dim=-2)

    global_trans = nn[..., -4:-1]

    rot = nn[..., 0:angle_repr["dim"]]

    if offset:
        global_pose = rot + \
            torch.tensor([1, 0, 0, 0, 1, 0], device=nn.device, dtype=nn.dtype)
    else:
        global_pose = rot

    if(only_z):
        bs, nl = nn.shape[:2]
        rot_33 = arepr.convertPose(global_pose, angle_repr, arepr.MAT).reshape(
            bs, nl, 9)
        rot_eu_noz = arepr.convertPose(rot_33, arepr.MAT, arepr.EUL)

        rot_eu_noz[..., 1] = 0
        rot_33_noz = arepr.convertPose(rot_eu_noz, arepr.EUL, arepr.MAT).reshape(
            -1, 3, 3)
        rot_33 = rot_33.reshape(-1, 3, 3)
        rot_33 = torch.bmm(rot_33_noz.transpose(-1, -2),
                           rot_33).reshape(bs, nl, 9)
        global_pose = arepr.convertPose(rot_33, arepr.MAT, angle_repr)

    # if(mode == "interpol"):
    #     global_pose = arepr.convertPose(global_pose, angle_repr, arepr.QUAT)
    #     global_pose = arepr.slerp(
    #         global_pose[..., 0, :], global_pose[..., 1, :], tinterp)
    #     global_pose = arepr.convertPose(global_pose, arepr.QUAT, angle_repr)
    #     global_trans = global_trans[..., 0, :] * \
    #         (1-tinterp) + tinterp*global_trans[..., 1, :]

    return torch.cat((global_pose, global_trans), dim=-1).detach()


def apply_transfo(subseqs, transfos, transfo_angle_repr=arepr.SIX, model_angle_repr=arepr.SIX, inv=False, offset=True):

    rot = transfos[..., :transfo_angle_repr["dim"]].clone()
    trans = transfos[..., -3:].clone()

    bs, nl, n_timestamps = rot.shape[:3]

    rot_33 = arepr.convertPose(rot, transfo_angle_repr, arepr.MAT).reshape(
        bs, nl, n_timestamps, 3, 3)

    subseqs_cp = subseqs.clone()

    if(inv):
        rot_33 = rot_33.transpose(-1, -2)

    if offset:
        offset = torch.tensor([1, 0, 0, 0, 1, 0],
                              device=subseqs.device, dtype=subseqs.dtype)
    else:
        offset = 0

    root_orient = arepr.convertPose(subseqs_cp[..., :model_angle_repr["dim"]]+offset,
                                    model_angle_repr, arepr.MAT).reshape(bs, nl, n_timestamps, 3, 3)

    subseqs_cp[..., :model_angle_repr["dim"]] = arepr.convertPose(torch.matmul(
        rot_33, root_orient).reshape(bs, nl, n_timestamps, 9), arepr.MAT, model_angle_repr) - offset

    
    if(inv):
        transseq = subseqs_cp[..., -3:] - trans
        pos_seq =  subseqs_cp[..., :-3]
        
        transseq = torch.matmul(rot_33,transseq[..., None])[..., 0]
        subseqs_cp = torch.cat([pos_seq,transseq],dim=-1)
       
    else:
        transseq = subseqs_cp[..., -3:]
        pos_seq =  subseqs_cp[..., :-3]
        
        transseq = torch.matmul(rot_33,transseq[..., None])[..., 0]
        transseq = transseq + trans
        subseqs_cp = torch.cat([pos_seq,transseq],dim=-1)
    return subseqs_cp
