import numpy as np
import torch
import os
import re
import json

from losses.losses import *


def write_config(configuration, path):

    del configuration["dataset"]["sampling_fn"]
    del configuration["dtype"]
    json.dump(configuration, open(path, "w+"))


def list_files_structured(folder):
    """List all filenames in a folder recursively,
    keep the arborescence of the folder

    Args:
        folder (str): Path to the desired folder

    Returns:
        dict: Recursive dictionnary where each key is a folder, each folder is represented by a dictionnary
        and has a "files" key if there is any files in it
    """
    filenames = dict()

    for r, d, f in os.walk(folder):
        position = r.split(os.sep)
        current = filenames
        for p in position:
            try:
                current = current[p]
            except:
                current[p] = dict()
                current = current[p]

        current["files"] = f

    return filenames


def list_files(folder, pattern=""):
    """List all files from a given folder

    Args:
        folder (str): Path to the desired folder
        pattern (str, optional): A pattern that is looked for in the filenames. Defaults to "".

    Returns:
        list: A list of strings, paths to every file from the folder
    """
    files = []
    for r, d, f in os.walk(folder):
        for file in f:
            if re.search(pattern, file) is not None:
                files.append(os.path.join(r, file))

    return files


def list_subfolders(folder, pattern=""):
    """List all files from a given folder

    Args:
        folder (str): Path to the desired folder
        pattern (str, optional): A pattern that is looked for in the filenames. Defaults to "".

    Returns:
        list: A list of strings, paths to every folder from the folder
    """
    folders = []
    for r, d, f in os.walk(folder):
        for folder in d:
            if re.search(pattern, folder) is not None:
                folders.append(os.path.join(r, folder))

    return folders
