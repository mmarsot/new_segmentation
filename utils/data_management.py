import torch
from data.smpl_sequence import SMPL_Sequence
from data.amass_dataset import AmassDataset
from body_models.smpl_model import SMPLH
from utils.file_management import list_files
import tqdm
import sys
import json
import os
import random 
from torch.utils.data import DataLoader


def create_dataset(configuration, split, shuffle=True, nsample="all", min_duration=1.0):
    labels2num = dict()
    smpl_sequences = load_sequences(
        configuration[split]["folder"], dtype=configuration["dtype"],
        device=configuration["device"], load=configuration["dataset"]["load"],
        pose_hand=configuration["dataset"]["pose_hand"],
        angle_repr=configuration["dataset"]["joint_repr"],
        condition_fn=lambda s: s.duration() >= min_duration,
        with_babel=False, babel_path='../../AMASSnhpp/babel_v1.0_release/',
        labels2num=labels2num)
    print(labels2num)
    print(len(smpl_sequences))
    if(nsample != "all"):
        random.seed(0)
        smpl_sequences = random.sample(smpl_sequences, nsample)

    smplh = SMPLH(configuration)
    dataset = AmassDataset(
        smpl_sequences, configuration["dataset"], smplh, dtype=configuration["dtype"], labels2num=labels2num)
    print("Split:%s, %d items" % (split, len(dataset)))
    loader = DataLoader(
        dataset, batch_size=configuration[split]["batch_size"], shuffle=shuffle,num_workers=8)

    return dataset, loader

def load_sequences(folder, condition_fn=lambda s: True,
                   dtype=torch.float64, device="cuda", load=True, pose_hand=True,
                   angle_repr={"name": "aa", "dim": 3},
                   joints=list(range(22)),
                   with_tqdm=True,
                   labels2num=dict(),
                   with_babel=False,
                   babel_path=None):
    """Load sequences from a file path

    Args:
        folder (str): folder containing the npz sequences
        condition_fn (function, optional): A condition function toselect the sequence. Defaults to lambda s:True.
        dtype (optional): The data type of the sequences. Defaults to torch.float64.
        device (str, optional): The pytorch device for the sequences. Defaults to "cuda".
        load (bool, optional): Whether to load or not the data on the device. Defaults to True.
        pose_hand (bool, optional): Whether or nto the data includes SMPL hand pose. Defaults to True.
        angle_repr (dict, optional): Angle representation of the input data. Defaults to {"name": "aa", "dim": 3}.
        with_tqdm (bool, optional): Activate tqdm or not. Defaults to True.

    Returns:
        list: The sequences as SMPL_Sequence objects
    """

    smpl_files = list_files(folder, ".npz")

    smpl_sequences = []
    ignored_files = []
    if(with_tqdm):
        wrapper = tqdm.tqdm
    else:
        def wrapper(x, file=None): return x

    if(with_babel):

        babel_data = {
            **json.load(open(os.path.join(babel_path, "train.json"))),
            **json.load(open(os.path.join(babel_path, "val.json")))
        }

        babel_processed_frame = dict()
        babel_processed_seq = dict()
        for key in babel_data:
            filez = babel_data[key]["feat_p"][babel_data[key]
                                              ["feat_p"].find("/")+1:]
            babel_processed_frame[filez] = babel_data[key]["frame_ann"]
            babel_processed_seq[filez] = babel_data[key]["seq_ann"]
    labels2num[None] = 0

    for f in wrapper(smpl_files, file=sys.stdout):
        if('shape.npz' in f):
            ignored_files.append(f)
            continue

        s = SMPL_Sequence(f, load_smpl=load, dtype=dtype,
                          device=device, pose_hand=pose_hand, angle_repr=angle_repr, joints=joints)

        if(s.data is None):
            print(s.npz_file)
        s.data["labels"] = torch.zeros(
            (s.number_of_frames(), 1), device=s.device)

        if(with_babel and f.replace(folder+"/", "") in babel_processed_frame.keys()):
            k = f.replace(folder+"/", "")

            # peut etre multi action a voir ce qu on fait
            # if(babel_processed_seq[k]["labels"][0]["act_cat"] != None):

            #     if babel_processed_seq[k]["labels"][0]["act_cat"][0] not in labels2num.keys():
            #         labels2num[babel_processed_seq[k]["labels"][0]
            #                    ["act_cat"][0]] = len(labels2num.keys())
            #     s.data["labels"][:,
            #                      :] = labels2num[babel_processed_seq[k]["labels"][0]["act_cat"][0]]
            if(babel_processed_frame[k] != None):
                for i in range(s.number_of_frames()):
                    ti = i/s.framerate()
                    for seg in babel_processed_frame[k]["labels"]:
                        if ti > seg["start_t"] and ti <= seg["end_t"]:

                            if seg["act_cat"][0] not in labels2num.keys():
                                labels2num[seg["act_cat"][0]] = len(
                                    labels2num.keys())

                            s.data["labels"][i,
                                             :] = labels2num[seg["act_cat"][0]]

        if(condition_fn(s)):
            smpl_sequences.append(s)
        else:
            ignored_files.append(f)
            continue

    return smpl_sequences

