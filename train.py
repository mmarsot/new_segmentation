from body_models.smpl_model import SMPLH
import json
from normalizers.normalizer_smpl import SMPL_normalizer
from models import get_model
import torch
import time
import os
import argparse
from human_body_prior.tools.omni_tools import makepath
from shutil import copyfile
from utils.data_management import create_dataset

def print_current_losses(epoch, max_epochs, it, max_iters, losses):
        """Print current losses on console.
        Input params:
            epoch: Current epoch.
            max_epochs: Maximum number of epochs.
            iter: Iteration in epoch.
            max_iters: Number of iterations in epoch.
            losses: Training losses stored in the format of (name, float) pairs
        """
        message = '[epoch: {}/{}\t, iter: {}/{} \t] '.format(
            epoch, max_epochs, it, max_iters)
        for k, v in losses.items():
            message += '{0}: {1:.6f} '.format(k, v)

        print(message, flush=True)  # print the message


def annealing(epoch, weight, start, end):
    if epoch >= start:
        if epoch >= end:
            return weight
        else:
            return((epoch-start)/(end-start))*weight

    return 0


def average_losses(losses):

    losses_name = losses[0]["losses"].keys()
    avg_loss = {k: 0 for k in losses_name}

    bs_sum = 0
    for d in losses:

        bs = d["bs"]
        bs_sum += bs

        for k in losses_name:
            avg_loss[k] = avg_loss[k] + d["losses"][k] * bs

    for k in losses_name:
        avg_loss[k] = avg_loss[k].detach().cpu() / bs_sum

    return avg_loss


def train(configuration, args):


    train_dataset, train_loader = create_dataset(
        configuration, "training", nsample=configuration["training"]["nsample"], min_duration=configuration["dataset"]["min_duration"])
    train_dataset_size = len(train_dataset)
    print('The number of training samples = {0}'.format(train_dataset_size))

    val_dataset, vald_loader = create_dataset(
        configuration, "validation", min_duration=configuration["dataset"]["min_duration"])
    val_dataset_size = len(val_dataset)
    print('The number of validation samples = {0}'.format(val_dataset_size))

    print('Initializing model...')
    normalizer = SMPL_normalizer(configuration)
    body_model = SMPLH(configuration)

    model_class = get_model(configuration)

    model = model_class(configuration, normalizer, body_model).to(
        configuration["device"])
    if(configuration["training"]["initialization"] != ""):
        model.load_state_dict(torch.load(
            configuration["training"]["initialization"]),strict=False)
    print("Model has %d parameters" % model.get_n_params())
    current_configuration = configuration

    if(args.pointcloud_config != "" and args.pointcloud_config is not None):
        pointcloud_config = json.load(open(args.pointcloud_config))
    else:

        model.initialize_optimizer(configuration)
        pointcloud_config = None
    if(args.pointcloud_start <= 0):
        model.add_pointcloud_encoder(pointcloud_config)
        current_configuration = pointcloud_config
        model.optimizer = torch.optim.Adam(
            model.pointcloud_encoder.parameters(), lr=current_configuration["training"]['lr'])
        model.scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            model.optimizer, mode='min', factor=0.1, patience=20,
            threshold=0.0001, threshold_mode='rel', cooldown=50,
            min_lr=1e-7, eps=1e-08, verbose=True)
    else:
        model.initialize_optimizer(configuration)
    if(current_configuration["training"]["initialization"] != ""):
        model.load_state_dict(torch.load(
            current_configuration["training"]["initialization"]),strict=False)
        if(not pointcloud_config):
            model.optimizer.load_state_dict(torch.load(
                current_configuration["training"]["initialization"].replace("model", "optim")))
            for g in model.optimizer.param_groups:
                g['lr'] = current_configuration["training"]["lr"]

    starting_epoch = args.start
    num_epochs = current_configuration['training']['max_epochs']

    makepath(current_configuration["experiment"]["folder"])

    try:
        all_train_losses = torch.load(os.path.join(
            current_configuration["experiment"]["folder"], 'train_loss.pt'))
        all_vald_losses = torch.load(os.path.join(
            current_configuration["experiment"]["folder"], 'vald_loss.pt'))
    except:
        all_vald_losses = dict()
        all_train_losses = dict()

    for epoch in range(starting_epoch, num_epochs):

        if(pointcloud_config != None and epoch >= args.pointcloud_start and not model.pointcloud_encoding):
            model.add_pointcloud_encoder(pointcloud_config)
            model.optimizer = torch.optim.Adam(
                model.pointcloud_encoder.parameters(), lr=pointcloud_config["training"]['lr'])
            model.scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
                model.optimizer, mode='min', factor=0.1, patience=50,
                threshold=0.0001, threshold_mode='rel', cooldown=50,
                min_lr=0.0, eps=1e-08, verbose=True)
            current_configuration = pointcloud_config
            makepath(current_configuration["experiment"]["folder"])

        train_losses, vald_losses = [], []
        out_file = current_configuration["experiment"]["out_file"]
        epoch_start_time = time.time()  # timer for entire epoch

        model.train()
        # model.masking_fn = binary_masking
        kl_target=current_configuration["training"]["kl_weight"]
        torch.autograd.set_detect_anomaly(True)
        loss = 0
        for i, data in enumerate(train_loader):  # inner loop within one epoch
            data = {k:data[k].to(model.device) for k in data.keys()}
            model.set_input(data,
                            data_params=current_configuration["dataset"])
  

            if(epoch > current_configuration["training"]["sparse_training"]):
                model.encoder.sparse = True
                model.encoder.sparse_N = 10

            model.forward()
            current_configuration["training"]["kl_weight"]=annealing(epoch,kl_target,10,50)
            
            model.compute_loss(epoch, current_configuration["training"])
           
            model.optimize_parameters()

            train_losses.append(
                {"losses": model.get_loss_details(), "bs": data["poses"].shape[0]})

            if (i+1) % current_configuration["training"]["printout_freq"] == 0:
                print_current_losses(
                    epoch, num_epochs, i+1, len(train_loader), average_losses(train_losses))
                
                try:
                    print("Transition times:", *(
                        (model.intermediate_variables["segm_params"][0][..., 0])).detach().tolist())
                    print("Durations:", *(
                        (model.intermediate_variables["segm_params"][0][..., 1])).detach().tolist())
                    print("Input Duration:",
                        (model.intermediate_variables["sampled_data"]["timestamp"][0][..., -1]).detach().tolist())
                except:
                    pass
            loss += model.loss.detach().cpu()*data["poses"].shape[0]

        print_current_losses(
            epoch, num_epochs, i+1, len(train_loader), average_losses(train_losses))

        if(epoch > configuration["training"]["mesh_start"]):
            model.scheduler.step(loss/len(train_dataset))

        model.eval()
        for i, data in enumerate(vald_loader):
            data = {k:data[k].to(model.device) for k in data.keys()}
            model.set_input(data,
                            data_params=current_configuration["dataset"])

            with torch.no_grad():
                model.forward()
            vald_losses.append(
                {"losses": model.get_loss_details(), "bs": data["poses"].shape[0]})

        train_losses = average_losses(train_losses)
        vald_losses = average_losses(vald_losses)

        for k in model.get_loss_details().keys():
            if k in all_train_losses.keys():
                all_train_losses[k].append(train_losses[k])
                all_vald_losses[k].append(vald_losses[k])
            else:
                all_train_losses[k] = [train_losses[k]]
                all_vald_losses[k] = [vald_losses[k]]


        if(epoch % 100 == 0):
            model.save_networks(out_file.replace(".pt", "%d.pt" % epoch))

       
        if(epoch % current_configuration["training"]["save_freq"] == 0):
            model.save_networks(out_file)

        print_current_losses(
            epoch, num_epochs, len(vald_loader), len(vald_loader), vald_losses)

        if(epoch % current_configuration["training"]["save_freq"] == 0):
            torch.save(all_train_losses, os.path.join(
                current_configuration["experiment"]["folder"], 'train_loss.pt'))
            torch.save(all_vald_losses, os.path.join(
                current_configuration["experiment"]["folder"], 'vald_loss.pt'))
            if(current_configuration["training"]["checkpoint_folder"] != ""):
                makepath(
                    current_configuration["training"]["checkpoint_folder"])
                model.save_checkpoint(
                    current_configuration["training"]["checkpoint_folder"], epoch)
      
        print('End of epoch {} / {} \t Time Taken: {:.4f} sec, Current lr:{:.8f}'.format(epoch,
                                                                                         num_epochs, time.time() - epoch_start_time, model.optimizer.param_groups[0]["lr"]))

    torch.save(all_train_losses, os.path.join(
        current_configuration["experiment"]["folder"], 'train_loss.pt'))
    torch.save(all_vald_losses, os.path.join(
        current_configuration["experiment"]["folder"], 'vald_loss.pt'))


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("config", nargs="?",
                        help="path to a configuration file, defaults to config.json", default="config.json")

    parser.add_argument("--pointcloud_config", nargs="?",
                        help="path to a configuration for a pointcloud encoder", default="")
    parser.add_argument("--pointcloud_start",  type=int, nargs="?", default=1000000,
                        help="path to a configuration for a pointcloud encoder")
    parser.add_argument("--start",  type=int, nargs="?", default=0,
                        help="starting epoch")

    args = parser.parse_args()

    configuration = json.load(open(args.config))
    configuration["dtype"] = torch.float32
    #torch.set_default_tensor_type('torch.cuda.FloatTensor')

    makepath(configuration["experiment"]["folder"])
    if(args.config != os.path.join(
            configuration["experiment"]["folder"], "config.json")):
        copyfile(args.config, os.path.join(
            configuration["experiment"]["folder"], "config.json"))

    torch.set_default_dtype(configuration["dtype"])
    train(configuration, args)
