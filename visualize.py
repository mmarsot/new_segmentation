import torch
from models import get_model
import argparse
from body_models.smpl_model import SMPLH
from normalizers.normalizer_smpl import SMPL_normalizer
from train import create_dataset
import open3d as o3d
import open3d.visualization.gui as gui
import numpy as np
import copy
import trimesh
import argparse
import matplotlib.pyplot as plt
import matplotlib
import json
import os
from human_body_prior.tools.omni_tools import makepath
torch.set_default_tensor_type('torch.cuda.FloatTensor')


def move_figure(f, x, y):
    """Move figure's upper left corner to pixel (x, y)"""
    backend = matplotlib.get_backend()
    if backend == 'TkAgg':
        f.canvas.manager.window.wm_geometry("+%d+%d" % (x, y))
    elif backend == 'WXAgg':
        f.canvas.manager.window.SetPosition((x, y))
    else:
        # This works for QT and GTK
        # You can also use window.setGeometry
        f.canvas.manager.window.move(x, y)


parser = argparse.ArgumentParser()
parser.add_argument("--config", type=str,
                    help="path to a configuration file, defaults to config.json", default="config.json")
parser.add_argument("--dual_config", type=str,
                    help="path to a configuration file, defaults to config.json", default="")

args = parser.parse_args()

configuration = json.load(open(args.config))


configuration["dtype"] = torch.float32
global smplh, cpu_faces
smplh = SMPLH(configuration)
cpu_faces = smplh.get_faces().cpu()

normalizer = SMPL_normalizer(configuration)

global model, dataset
model_class = get_model(configuration)

model = model_class(configuration, normalizer, smplh).to(
    configuration["device"])
model.initialize_optimizer(configuration)
model.pretrain(False)
if(args.dual_config != ""):
    dual_configuration = configuration = json.load(open(args.dual_config))
    model.add_dual(dual_configuration)
    print(dual_configuration["experiment"]["out_file"])
    model.load_state_dict(torch.load(
        dual_configuration["experiment"]["out_file"]),strict=False)
    dual_configuration["dtype"] = torch.float32
else:
    model.load_state_dict(torch.load(
        configuration["experiment"]["out_file"]),strict=False)
model.eval()
dataset, loader = create_dataset(
    configuration, "validation", min_duration=configuration["dataset"]["min_duration"], shuffle=False)
global colormap
colormap =plt.get_cmap("seismic")
global anim_state
anim_state = False

global t, timeline, scale
t = 0
scale = 1.0
timeline = None
global visu_input, visu_rec, generate
visu_input = True
visu_rec = True
generate = False

global figure_pca, figure_masks

figure_pca = plt.figure()

figure_pca.gca().set_xticks(
    list(range(configuration["model"]["encoder"]["nsegments"])))
figure_pca.gca().set_xticklabels(
    list(range(configuration["model"]["encoder"]["nsegments"])))

figure_pca.gca().set_yticks(
    list(range(configuration["model"]["encoder"]["nsegments"])))
figure_pca.gca().set_yticklabels(
    list(range(configuration["model"]["encoder"]["nsegments"])))

move_figure(figure_pca, 1300, 500)

if "segmentation" in configuration["model"]["type"]:
    figure_masks = plt.figure()
    move_figure(figure_masks, 1300, 1000)


def anim_switch():
    global anim_state

    anim_state = not anim_state


def show_latent():
    global model, pca, figure_pca, figure_masks, generate, dataset
    if not "segmentation" in configuration["model"]["type"]:
        return
   
    dist = model.intermediate_variables["latent_primitives"].reshape(-1,
                                                                   configuration["model"]["encoder"]["latent_dim"]).cpu().detach()
 
    dist = torch.cdist(dist, dist,p=2)

    figure_pca.clf()

    ax = figure_pca.gca()
    ax.matshow(dist, cmap=plt.cm.Blues)

    for i in range(dist.shape[1]):
        for j in range(dist.shape[0]):
            c = float(dist[j, i].item())
            ax.text(i, j, "%.02f" % c, va='center', ha='center')
        plt.draw()

    if(generate):
        model.intermediate_variables["sampled_data"]["timestamp"] = (torch.linspace(
            0, dataset.max_duration, 100))#/(dataset.max_duration)
        model.intermediate_variables["sampled_data"]["timestamp"] = model.intermediate_variables["sampled_data"]["timestamp"][None, ...]

    n_timestamps = model.intermediate_variables["sampled_data"]["timestamp"].shape[1]

    masks = model.decoder.create_masks(
        model.intermediate_variables["segm_params"][:,
                                                    :, None, :].repeat(1, 1, n_timestamps, 1),
        model.intermediate_variables["sampled_data"]["timestamp"][:, None:, None].repeat(
            1,  model.num_latents, 1, 1),
        eps=1e-7)

    figure_masks.clf()
    axm = figure_masks.gca()

    for m in masks[0]:

        axm.plot(model.intermediate_variables["sampled_data"]["timestamp"]
                 [0].cpu().detach(), m.cpu().detach())
        plt.draw()


def mesh_slider_fn(time):
    global folder, scale, current_mesh, t, smplh, generate, subseq_meshes, visu_input, input_mesh, i, visu_rec, figure_pca, figure_masks, slider_mesh, timeline

    t = time
    if(timeline is not None):
        timeline.remove()
    timeline = figure_masks.gca().vlines(t, 0, 1)

    timestamp = torch.tensor(t, device=smplh.device).reshape(-1, 1)

    morphology = model.intermediate_variables["sampled_data"]["betas"]
    data = model.decode(timestamp, morphology)

    if "segmentation" in configuration["model"]["type"]:
        subseqs = model.intermediate_variables["rec_subseqs"]
        durations = model.intermediate_variables["segm_params"][..., 1:2]

    # print("Durations:", durations)
    data = data.squeeze(dim=0)

    data_dict = dict()
    data_dict["betas"] = model.intermediate_variables["sampled_data"]["betas"].clone()

    data_dict["poses"] = data[:, :model.joint_repr["dim"]
                              * len(model.joints)]
    data_dict["trans"] = data[:, model.joint_repr["dim"]
                              * len(model.joints):]

    data_dict = model.normalizer.unnormalize(data_dict)

    mesh = smplh(data_dict, angle_repr=model.joint_repr,
                 joints=model.joints, use_hands=False)
    # slider_mesh.set_limits()
    if(visu_rec):
        current_mesh.vertices = o3d.utility.Vector3dVector(
            mesh.cpu().detach().numpy())
        
        # current_mesh.vertex["colors"] = o3d.core.Tensor(...)

    # color = [0.606, 0.606, 0.606]
    color = [0, 0, 0]
    nseg = 0
    if "segmentation" in configuration["model"]["type"]:
        subseqs = subseqs[0, :, 0, :]

        subseqs_dict = dict()
        subseqs_dict["betas"] = model.intermediate_variables["sampled_data"]["betas"].clone()
        subseqs_dict["poses"] = subseqs[:, :model.joint_repr["dim"] *
                                        len(model.joints)]
        subseqs_dict["trans"] = subseqs[:, model.joint_repr["dim"] *
                                        len(model.joints):]
        subseqs_dict = model.normalizer.unnormalize(subseqs_dict)

        subseq_vertices = smplh(subseqs_dict, angle_repr=model.joint_repr,
                                joints=model.joints, use_hands=False, squeeze=False)

        t0 = model.intermediate_variables["segm_params"][..., 0:1]
        d = model.intermediate_variables["segm_params"][..., 1]

        # print(model.segm_params, durations[0, :, 0, :])
        masks = model.decoder.create_masks(
            model.intermediate_variables["segm_params"], t)

        if(masks.max() > 0.1):
            color = seg_colors[masks.argmax()]
            nseg = masks.argmax()
        else:
            color = [0, 0, 0]

        figure_pca.gca().set_xticks(
            list(range(model.num_latents+1)))
        figure_pca.gca().set_xticklabels(
            list(range(model.num_latents+1)))

        figure_pca.gca().set_yticks(
            list(range(model.num_latents+1)))
        figure_pca.gca().set_yticklabels(
            list(range(model.num_latents+1)))

        for j, s in enumerate(subseq_vertices):

            # subseq_meshes[j].vertices = o3d.utility.Vector3dVector(
            #     s.cpu().detach().numpy())

            if(t > t0.squeeze(dim=0)[j, 0] and t < (t0.squeeze(dim=0)+d)[j, 0]):
                #subseq_meshes[j].paint_uniform_color([0.0, 1.0, 0.0])

                nseg += 1
                figure_pca.gca().get_xticklabels()[
                    j+1].set_color([0.0, 1.0, 0.0])
                figure_pca.gca().get_yticklabels()[
                    j+1].set_color([0.0, 1.0, 0.0])

            elif t < t0.squeeze(dim=0)[j, 0]:
               # subseq_meshes[j].paint_uniform_color([0.5, 0.5, 0.0])
                figure_pca.gca().get_xticklabels()[
                    j+1].set_color([0.5, 0.5, 0.0])
                figure_pca.gca().get_yticklabels()[
                    j+1].set_color([0.5, 0.5, 0.0])

            elif t > (t0.squeeze(dim=0)+d)[j, 0]:
              #  subseq_meshes[j].paint_uniform_color([1.0, 0.0, 0.0])
                figure_pca.gca().get_xticklabels()[
                    j+1].set_color([1.0, 0.0, 0.0])
                figure_pca.gca().get_yticklabels()[
                    j+1].set_color([1.0, 0.0, 0.0])

        plt.draw()

    if((not visu_input) or generate):
        input_mesh.vertices = o3d.utility.Vector3dVector(np.zeros([6890, 3]))
    else:
     
        smpl_data = dataset.smpl_dataset[i].to(smplh.device).frame_data(
            seq_data["interval"][0]/seq_data["mocap_framerate"] + scale*t*configuration["dataset"]["max_duration"], unit="s")#-configuration["dataset"]["margin"])+configuration["dataset"]["margin"], unit="s")
        normalized_trans = model.normalizer.normalize_trans(smpl_data)
        normalized_smpl_repr = torch.cat(
            (smpl_data["poses"], normalized_trans), dim=-1)


        smpl_data["poses"] = normalized_smpl_repr[:-3]
        smpl_data["trans"] = normalized_smpl_repr[-3:]
        smpl_data["trans"] = model.normalizer.unnormalize_trans(smpl_data)
        new_verts = smplh(
            smpl_data, angle_repr=dataset.smpl_dataset[i].angle_repr, joints=dataset.smpl_dataset[i].joints, use_hands=False)

        input_mesh.vertices = o3d.utility.Vector3dVector(
            new_verts.cpu().detach().numpy())
        if "segmentation" in configuration["model"]["type"]:
            # color = [c/(nseg+1e-5) for c in color]
            input_mesh.paint_uniform_color(color)

        errors =torch.sqrt(torch.sum((mesh-new_verts)**2,dim=-1))
        errors = errors*1000
        errors = torch.clamp(torch.tensor(errors),0,150)/150
        errors= colormap(errors.cpu().numpy())
        
        current_mesh.vertex_colors = o3d.utility.Vector3dVector(
            errors[...,:3])
    # update()


def change_sparsity(new_sparsity):
    global model
    model.encoder.sparse = True
    model.encoder.sparse_N = int(new_sparsity)
    validate_change()


def change_nseg(new_nseg):
    global model, subseq_meshes, seg_colors

    model.encoder.num_latents = int(new_nseg)
    model.encoder.init_enc = model.encoder.get_p_enc(
        model.encoder.num_latents, model.encoder.latent_seg_dim)
    model.decoder.num_latents = int(new_nseg)
    model.decoder.penc = model.decoder.get_p_enc(
        model.decoder.num_latents, model.decoder.latent_seg_dim)
    model.num_latents = int(new_nseg)
    subseq_meshes = [copy.deepcopy(mesh.as_open3d) for i in range(
        model.num_latents)]
    seg_colors = [[(i % 3)/2.0, ((i+1) % 3)/2.0, ((i+2) % 3)/2.0]
                  for i in range(model.num_latents)]
    validate_change()


def change_start(new_start):
    global start, slider_end
    start = int(new_start)-1
    slider_end.set_limits(start, slider_end.get_maximum_value)


def change_end(new_end):
    global end
    end = int(new_end)-1


def validate_change():
    global i, model, t, smplh, start, end, seq_data, model, slider_mesh, slider_sparsity, generate, dataset, scale
    seq_data = dataset.get_dict(i, start, end)

    for key in "poses", "trans", "timestamp", "betas", "nof":
        try:
            seq_data[key] = seq_data[key][None, ...].cuda()
        except:
            print(key)

    scale = max(1.0, seq_data["timestamp"][..., seq_data["nof"]-1].item())
    seq_data["timestamp"] = seq_data["timestamp"] #/ \
        #max(1.0, seq_data["timestamp"][..., seq_data["nof"]-1].item())

    model.set_input(seq_data,
                    data_params=configuration["dataset"])

    if(not generate):
        slider_mesh.set_limits(
            model.intermediate_variables["sampled_data"]["timestamp"][0, 0].item(), model.intermediate_variables["sampled_data"]["timestamp"][0, -1].item())
        with torch.no_grad():
            model.forward()

        slider_sparsity.set_limits(1, seq_data["nof"][0][0].int())

    if "segmentation" in configuration["model"]["type"]:
        print("Segmentation")
        print("t0:", model.intermediate_variables["segm_params"][..., 0])
        print("d",  model.intermediate_variables["segm_params"][..., 1])
    mesh_slider_fn(t)
    show_latent()


def next():
    global i, model, t, smplh, start, end, seq_data, slider_start, slider_end, model, generate

    if(not generate):
        seq_data = dataset.__getitem__(i+1)
        seq_data = {k:seq_data[k].to(model.device) for k in seq_data.keys()}
        i = i+1
        start = seq_data["interval"][0]
        end = seq_data["interval"][1]
        slider_start.int_value = int(start)
        slider_end.int_value = int(end)-1
        for key in "poses", "trans", "timestamp", "betas", "nof":
            try:
                seq_data[key] = seq_data[key][None, ...].cuda()
            except:
                print(key)

        model.set_input(seq_data,
                        data_params=configuration["dataset"])

        slider_mesh.set_limits(
            model.intermediate_variables["sampled_data"]["timestamp"][0, 0].item(), model.intermediate_variables["sampled_data"]["timestamp"][0, -1].item())
        with torch.no_grad():
            model.forward()
        slider_sparsity.set_limits(1, seq_data["nof"][0][0].int())
    else:

        model.generate(
            model.intermediate_variables["sampled_data"]["betas"], model.intermediate_variables["latent_and_seg"][:, 0, :])
        slider_mesh.set_limits(0, 10)

    if "segmentation" in configuration["model"]["type"]:
        print("Segmentation")
        print()
        print("t0:", model.intermediate_variables["segm_params"][..., 0])
        print("d",  model.intermediate_variables["segm_params"][..., 1])
    mesh_slider_fn(t)
    validate_change()

    # show_latent()
    # print(model.masks,model.global_transfo)


def update():
    global vis, current_mesh, subseq_vis, subseq_meshes

    vis.update_geometry(current_mesh)
    vis.update_renderer()
    if "segmentation" in configuration["model"]["type"]:
        for svis, smesh in zip(subseq_vis, subseq_meshes):
            svis.update_geometry(smesh)
            svis.update_renderer()


def visu_input_fn():
    global visu_input, input_mesh, seq_data, scale
    
    visu_input = not visu_input
    if(not visu_input) or generate:
        input_mesh.vertices = o3d.utility.Vector3dVector(np.zeros([6890, 3]))

    else:
        smpl_data = dataset.smpl_dataset[i].to(smplh.device).frame_data(
            seq_data["interval"][0]/seq_data["mocap_framerate"] + scale*t*configuration["dataset"]["max_duration"], unit="s")#*(configuration["dataset"]["max_duration"]-configuration["dataset"]["margin"])+configuration["dataset"]["margin"], unit="s")

        new_verts = smplh(
            smpl_data, angle_repr=dataset.smpl_dataset[i].angle_repr, joints=dataset.smpl_dataset[i].joints, use_hands=False)
        # new_mesh = trimesh.Trimesh(vertices=new_verts.cpu().detach(),
        #                            faces=cpu_faces, process=False).as_open3d
        input_mesh.vertices = o3d.utility.Vector3dVector(new_verts.cpu().detach().numpy())
        input_mesh.paint_uniform_color([0.606, 0.606, 0.606])

    # visualize_sequence(dataset.smpl_dataset[i],start=start,end=end,body_model=smplh)


def generate_fn():
    global generate
    generate = True
    next()


def next_n():
    global generate
    generate = False
    next()


def visu_rec_fn():
    global visu_rec, current_mesh, t, generated

    visu_rec = not visu_rec

    if(not visu_rec):
        current_mesh.vertices = o3d.utility.Vector3dVector(np.zeros([6890, 3]))

    else:
        mesh_slider_fn(t)

def save():
    global model,i
    
    model.forward()
    rec = model.intermediate_variables["rec_seq"]
    
    meshes = model.body_model(
            model.normalizer.unnormalize({"poses": rec[..., :-3].reshape(rec.shape[1], -1),
                                          "trans": rec[..., -3:].reshape(rec.shape[1], -1),
                                          "betas": model.intermediate_variables["sampled_data"]["betas"].repeat(rec.shape[1], 1),
                                          "dmpls": None}),
            angle_repr=model.joint_repr,
            joints=model.joints,
            trans=True,
            pose2rot=False)
    
    meshes_input = model.body_model(
            model.normalizer.unnormalize( {"poses": model.intermediate_variables["sampled_data"]["frame_repr"][..., :-3].reshape(rec.shape[1], -1),
                                          "trans": model.intermediate_variables["sampled_data"]["frame_repr"][..., -3:].reshape(rec.shape[1], -1),
                                          "betas": model.intermediate_variables["sampled_data"]["betas"].repeat(rec.shape[1], 1),
                                          "dmpls": None}),
            angle_repr=model.joint_repr,
            joints=model.joints,
            trans=True,
            pose2rot=False)
    makepath(os.path.join(
            configuration["experiment"]["folder"],"generalization","output", "%d"%i))
    makepath(os.path.join(
            configuration["experiment"]["folder"],"generalization","input", "%d"%i))
    for k, v in enumerate(meshes):
       
        eq_mesh = trimesh.Trimesh(vertices=v.detach().cpu().numpy(
        ), faces=model.body_model.get_faces().detach().cpu().numpy())
        eq_mesh.export(os.path.join(
            configuration["experiment"]["folder"], "generalization","output","%d"%i, "mesh%d.obj"%k))
    for k, v in enumerate(meshes_input):
       
        eq_mesh = trimesh.Trimesh(vertices=v.detach().cpu().numpy(
        ), faces=model.body_model.get_faces().detach().cpu().numpy())
        eq_mesh.export(os.path.join(
            configuration["experiment"]["folder"], "generalization","input","%d"%i, "mesh%d.obj"%k))
    next()
global i, start, end, seq_data
i = 0
seq_data = dataset.__getitem__(i)
seq_data = {k:seq_data[k].to(model.device) for k in seq_data.keys()}
start = seq_data["interval"][0]
end = seq_data["interval"][1]


for key in "poses", "trans", "timestamp", "betas", "nof":
    try:
        seq_data[key] = seq_data[key][None, ...].cuda()
    except:
        print(key)

model.set_input(seq_data,
                data_params=configuration["dataset"])


global input_mesh, seg_colors

smpl_data = dataset.smpl_dataset[i].to(smplh.device).frame_data(
    seq_data["interval"][0]/seq_data["mocap_framerate"] + t, unit="s")#*(configuration["dataset"]["max_duration"]-configuration["dataset"]["margin"])+configuration["dataset"]["margin"], unit="s")


input_mesh = smplh(
    smpl_data, 
    angle_repr=dataset.smpl_dataset[i].angle_repr, 
    joints=dataset.smpl_dataset[i].joints, use_hands=False)



input_mesh = trimesh.Trimesh(vertices=input_mesh.cpu().detach(),
                             faces=cpu_faces, process=False).as_open3d

if "segmentation" in configuration["model"]["type"]:
    input_mesh.paint_uniform_color([0.606, 0.606, 0.606])
    seg_colors = [[(i % 3)/2.0, ((i+1) % 3)/2.0, ((i+2) % 3)/2.0]
                  for i in range(model.num_latents)]


with torch.no_grad():

    model.forward()


gui.Application.instance.initialize()

window = gui.Application.instance.create_window(
    width=500, height=200, x=1300, y=100)


timestamp = torch.tensor(t, device=model.intermediate_variables["sampled_data"]["timestamp"].device,
                         dtype=model.intermediate_variables["sampled_data"]["timestamp"].dtype).reshape(-1, 1)

morphology = model.intermediate_variables["sampled_data"]["betas"]
data = model.decode(timestamp, morphology)

if "segmentation" in configuration["model"]["type"]:
    subseqs = model.intermediate_variables["rec_subseqs"]
    durations = model.intermediate_variables["segm_params"][..., 1:2]

data_dict = copy.deepcopy(model.intermediate_variables["sampled_data"])
data_dict["poses"] = data[..., :model.joint_repr["dim"]
                          * len(model.joints)].squeeze(dim=1)
data_dict["trans"] = data[..., model.joint_repr["dim"]
                          * len(model.joints):].squeeze(dim=1)

data_dict = model.normalizer.unnormalize(data_dict)

mesh = smplh(data_dict, angle_repr=model.joint_repr,
             joints=model.joints, use_hands=False)
mesh = trimesh.Trimesh(vertices=mesh.cpu().detach(),
                       faces=cpu_faces, process=False)


global current_mesh, subseq_meshes
current_mesh = mesh.as_open3d
current_mesh.paint_uniform_color([1, 0.706, 0])
if "segmentation" in configuration["model"]["type"]:
    subseq_meshes = [copy.deepcopy(mesh.as_open3d) for i in range(
        configuration["model"]["encoder"]["nsegments"])]

    subseqs = subseqs[0, :, 0, :]

    subseqs_dict = dict()
    subseqs_dict["betas"] = model.intermediate_variables["sampled_data"]["betas"].clone()
    subseqs_dict["poses"] = subseqs[:, :model.joint_repr["dim"] *
                                    len(model.joints)]
    subseqs_dict["trans"] = subseqs[:, model.joint_repr["dim"] *
                                    len(model.joints):]

    subseqs_dict = model.normalizer.unnormalize(subseqs_dict)

    # subseq_vertices = smplh(subseqs_dict, angle_repr=model.joint_repr,
    #                         joints=model.joints, use_hands=False, squeeze=False)

    # # for j, m in enumerate(subseq_vertices):

    #     subseq_meshes[j] = trimesh.Trimesh(vertices=m.cpu().detach(),
    #                                        faces=cpu_faces, process=False).as_open3d

    show_latent()

global slider_mesh
slider_mesh = gui.Slider(gui.Slider.DOUBLE)
slider_mesh.set_limits(0, 10)
slider_mesh.set_on_value_changed(mesh_slider_fn)
slider_start = gui.Slider(gui.Slider.INT)
slider_start.set_limits(1, dataset.smpl_dataset[i].number_of_frames())
slider_start.int_value = start
slider_start.set_on_value_changed(change_start)

slider_sparsity = gui.Slider(gui.Slider.INT)
slider_sparsity.set_limits(1, 200)
slider_sparsity.int_value = 100
slider_sparsity.set_on_value_changed(change_sparsity)
slider_nseg = gui.Slider(gui.Slider.INT)
slider_nseg.set_limits(2, 10*model.encoder.num_latents)
slider_nseg.int_value = model.encoder.num_latents
slider_nseg.set_on_value_changed(change_nseg)

slider_end = gui.Slider(gui.Slider.INT)
slider_end.set_limits(start, dataset.smpl_dataset[i].number_of_frames()-1)
slider_end.set_on_value_changed(change_end)
slider_end.int_value = end

b_valid = gui.Button("Confirm")
b_valid.set_on_clicked(validate_change)
grid_slider = gui.VGrid(9, 0.25)
grid_slider.add_child(gui.Label("Timestamp"))
grid_slider.add_child(slider_mesh)
grid_slider.add_child(gui.Label("Start"))
grid_slider.add_child(slider_start)
grid_slider.add_child(gui.Label("End"))
grid_slider.add_child(slider_end)
grid_slider.add_child(gui.Label("NFrames"))
grid_slider.add_child(slider_sparsity)
grid_slider.add_child(slider_nseg)
grid_slider.add_child(b_valid)

b_next = gui.Button("Next")
b_next.set_on_clicked(next_n)
b_vis = gui.Button("vis input")
b_vis.set_on_clicked(visu_input_fn)
b_vis_r = gui.Button("vis rec")
b_vis_r.set_on_clicked(visu_rec_fn)
b_anim = gui.Button("Anim")
b_anim.set_on_clicked(anim_switch)
b_generate = gui.Button("Generate")
b_generate.set_on_clicked(generate_fn)
b_save = gui.Button("Save")
b_save.set_on_clicked(save)

grid_all = gui.VGrid(1, 0.25)
grid_all.add_child(grid_slider)
grid_all.add_child(b_next)
grid_all.add_child(b_vis)
grid_all.add_child(b_vis_r)
grid_all.add_child(b_anim)
grid_all.add_child(b_generate)
grid_all.add_child(b_save)
window.add_child(grid_all)


validate_change()
global vis, subseq_vis
if "segmentation" in configuration["model"]["type"]:
    subseq_vis = [o3d.visualization.Visualizer()
                  for i in range(configuration["model"]["encoder"]["nsegments"])]
    # [svis.create_window(width=400, height=400, left=1920+i//2 *
    #                     400, top=i % 2*400) for i, svis in enumerate(subseq_vis)]
    # [svis.add_geometry(s_mesh)
    #  for svis, s_mesh in zip(subseq_vis, subseq_meshes)]

vis = o3d.visualization.Visualizer()
vis.create_window(width=1200, height=1200, left=0, top=50)
vis.add_geometry(current_mesh)
vis.add_geometry(input_mesh)

plt.ion()
plt.show()
a = 0
while True:

    if(anim_state):

        t = (t+0.005) % ((slider_mesh.get_maximum_value -
                          slider_mesh.get_minimum_value))

        mesh_slider_fn(t)

        slider_mesh.double_value = t
    if(visu_input and (not generate)):
        input_mesh.compute_triangle_normals()
        input_mesh.compute_vertex_normals()

    vis.update_geometry(input_mesh)

    if(visu_rec):
        current_mesh.compute_triangle_normals()
        current_mesh.compute_vertex_normals()
    vis.update_geometry(current_mesh)

    if not vis.poll_events():
        break
    vis.update_renderer()
    if "segmentation" in configuration["model"]["type"]:
        # for svis, smesh in zip(subseq_vis, subseq_meshes):
        #     smesh.compute_triangle_normals()
        #     smesh.compute_vertex_normals()
        #     svis.update_geometry(smesh)
            # if not svis.poll_events():
            #     break
            # svis.update_renderer()
        pass
    plt.pause(0.001)

gui.Application.instance.quit()
gui.Application.instance.run()

vis.close()
if "segmentation" in configuration["model"]["type"]:
    [svis.close() for svis in subseq_vis]
