import torch
from models import get_model
import argparse
from body_models.smpl_model import SMPLH
from normalizers.normalizer_smpl import SMPL_normalizer
from train import create_dataset
import open3d as o3d
import open3d.visualization.gui as gui
import numpy as np
import copy
import trimesh
import argparse
import matplotlib.pyplot as plt
import matplotlib
import json
import os
from human_body_prior.tools.omni_tools import makepath
import tqdm

parser = argparse.ArgumentParser()
parser.add_argument("--config", type=str,
                    help="path to a configuration file, defaults to config.json", default="config.json")
parser.add_argument("--dual_config", type=str,
                    help="path to a configuration file, defaults to config.json", default="")

args = parser.parse_args()

configuration = json.load(open(args.config))

configuration["dtype"] = torch.float32

smplh = SMPLH(configuration)
cpu_faces = smplh.get_faces().cpu()

normalizer = SMPL_normalizer(configuration)

model_class = get_model(configuration)

model = model_class(configuration, normalizer, smplh).to(
    configuration["device"])
model.initialize_optimizer(configuration)
model.pretrain(False)

model.load_state_dict(torch.load(
    configuration["experiment"]["out_file"]),strict=False)
model.eval()
dataset, loader = create_dataset(
    configuration, "training", min_duration=configuration["dataset"]["min_duration"], shuffle=False)

for i, data in tqdm.tqdm(enumerate(loader)):  # inner loop within one epoch
    data = {k:data[k].to(model.device) for k in data.keys()}
    model.set_input(data,
                    data_params=configuration["dataset"])


    model.forward()
    
    if i==0:
        durations  = model.intermediate_variables["sampled_data"]["timestamp"][0][..., -1] 
        seg_durations = model.intermediate_variables["segm_params"][0][..., 1]
    else:
        durations = torch.cat([durations,model.intermediate_variables["sampled_data"]["timestamp"][0][..., -1]],dim=0)
        seg_durations = torch.cat([seg_durations,model.intermediate_variables["segm_params"][0][..., 1]])
        
        